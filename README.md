# Integrantes: Fernández de la Torre Alejo, Monte Carolina

Junto con las clases del juego, se incluye un archivo predeterminedLegions.txt en la carpeta src\main\java\resources. El contenido del archivo puede ser modificado
para disponer de otras legiones predeterminadas en el juego, pero la localización de ese archivo NO debe ser cambiada.
<br>
<br>
<br>
## Decisions de diseño

Comenzando el proyecto, se quizo implementar el patrón Composite con tres clases de soldados normales y  una clase Legión que
puede contener a las otras, y a instancias de sí misma. El problema que se generó fue al implementar la clase Jugador,
la cual guardaba una instancia de Legión que almacenaría todos los soldados que el usuario quisiera usar. Esto rompe el patrón
Composite, ya que el usuario debería ser capaz de interactuar tanto con legiones como con soldados "sueltos". Para solucionarlo
se implementó una nueva clase Armada que almacenaría todo los soldados sueltos y legiones, manteniendo así el patrón Composite.
<br>
<br>
<br>
## Clases

**Soldier** *Clase abstracta*
Clase base de la que heredan los distintos tipos de soldados y la legión.
<br>
<br>
**Auxiliary - Legionnaire - Centurion - Legion** >>> *Heredan de Soldier*
Heredan métodos y atributos de la clase Soldier. El método attack() del soldado atacante calcula el daño que puede hacer y llama al método defend()
del soldado atacado para calcular el daño causado. Legion puede contener cualquier tipo de soldado y calcula el daño realizado al defensor llamando
al método attack() de cada soldado contenido.
<br>
<br>
**Army**
Clase que contiene todos los soldados de un jugador. Establece las interacciones entre Player y los hijos de Soldier.
<br>
<br>
**Player**
Representa al jugador, guarda su nombre y su armada, genera un resumen del estado del jugador.
<br>
<br>
**RomanBattle**
Estructura lógica del juego. Tira dados para decidir quién empieza, obtiene y lista las legiones predeterminadas, define la "tienda"
por la cual los jugadores reclutan soldados, permite a los jugadores atacar y cambia el turno entre jugadores.
<br>
<br>
**FileConverter**
Convierte un archivo de texto del formato FC a FPC y viceversa.
<br>
<br>
**BinaryHeap**
Estructura de datos implementada por Legion y Army para manipular los soldados.
<br>
<br>
**RomanBattleApplication**
Aplicación del juego.
<br>
<br>
**GameInterface**
Define la interfaz con la que interactúa el o los usuarios directamente.
<br>
<br>
<br>
## Conclusiones

Durante el desarrollo del proyecto se notó que, el hecho de que ciertos soldados puedan fallar sus ataques o esquivar otros dificulta
el desarrollo por TDD a la hora de probar las clases, ya que los métodos no devuelven un resultado fijo para los mismos parámetros.
Además, es posible implementar el mismo juego de tal manera que los soldados se manejen como cantidades en lugar de objetos en sí, pero
no se hizo ya que si, por ejemplo, cada soldado tuviera su propio nombre o ataque base distinto, dicho método no sería eficiente. En otras
palabras, no evolucionaría adecuadamente.