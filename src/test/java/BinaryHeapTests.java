
import org.junit.Test;

import logicFunctionality.BinaryHeap;
import logicFunctionality.BinaryHeapEmpty;

import static org.junit.Assert.*;

public class BinaryHeapTests {

	@Test
	public void createMaxBinaryHeapAndInsert() {
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(true);

		heap.insert(10);

		assertEquals("10", heap.toString());

		BinaryHeap<Integer> anotherHeap = new BinaryHeap<Integer>(true);

		anotherHeap.insert(10);
		anotherHeap.insert(12);

		assertEquals("12, 10", anotherHeap.toString());

		Integer[] array = { 3, 10, 6 };
		BinaryHeap<Integer> yetAnotherHeap = new BinaryHeap<Integer>(array, true);

		assertEquals("10, 3, 6", yetAnotherHeap.toString());
	}

	@Test
	public void insertInMaxLargeHeap() {
		Integer[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(array, true);

		heap.insert(21);

		assertEquals("21, 20, 15, 18, 19, 13, 14, 17, 9, 11, 2, 12, 6, 3, 7, 16, 8, 4, 1, 5, 10", heap.toString());
	}

	@Test
	public void removeMax() throws BinaryHeapEmpty {
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(true);

		heap.insert(10);
		heap.insert(12);
		heap.insert(15);
		heap.insert(9);

		assertEquals("15, 10, 12, 9", heap.toString());

		heap.remove();

		assertEquals("12, 10, 9", heap.toString());

		heap.remove();

		assertEquals("10, 9", heap.toString());
	}

	@Test
	public void removeMaxFromLargeHeap() throws BinaryHeapEmpty {
		Integer[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(array, true);

		heap.remove();

		assertEquals("19, 18, 15, 17, 11, 13, 14, 16, 9, 10, 2, 12, 6, 3, 7, 5, 8, 4, 1", heap.toString());
	}

	@Test
	public void heapSortLowToHigh() {
		Integer[] array = { 1, 7, 5, 7, 4, 8, 3 };

		BinaryHeap.heapSort(array, true);

		Integer[] comparisonArray = { 1, 3, 4, 5, 7, 7, 8 };

		assertArrayEquals(comparisonArray, array);
	}

	@Test
	public void heapSortLowToHigh2() {
		Integer[] array = { 9, 7, 14, 7, 4, 8, 3, 18, 72 };

		BinaryHeap.heapSort(array, true);

		Integer[] comparisonArray = { 3, 4, 7, 7, 8, 9, 14, 18, 72 };

		assertArrayEquals(comparisonArray, array);
	}

	@Test
	public void createMinBinaryHeapAndInsert() {
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(false);

		heap.insert(10);

		assertEquals("10", heap.toString());

		BinaryHeap<Integer> anotherHeap = new BinaryHeap<Integer>(false);

		anotherHeap.insert(12);
		anotherHeap.insert(10);

		assertEquals("10, 12", anotherHeap.toString());

		Integer[] array = { 10, 3, 6 };
		BinaryHeap<Integer> yetAnotherHeap = new BinaryHeap<Integer>(array, false);

		assertEquals("3, 10, 6", yetAnotherHeap.toString());
	}

	@Test
	public void insertInMinLargeHeap() {
		Integer[] array = { 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(array, false);

		heap.insert(0);

		assertEquals("0, 1, 6, 3, 2, 8, 7, 4, 12, 10, 19, 9, 15, 18, 14, 5, 13, 17, 20, 16, 11", heap.toString());
	}

	@Test
	public void removeMin() throws BinaryHeapEmpty {
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(false);

		heap.insert(10);
		heap.insert(12);
		heap.insert(15);
		heap.insert(9);

		assertEquals("9, 10, 15, 12", heap.toString());

		heap.remove();

		assertEquals("10, 12, 15", heap.toString());

		heap.remove();

		assertEquals("12, 15", heap.toString());
	}

	@Test
	public void removeMinFromLargeHeap() throws BinaryHeapEmpty {
		Integer[] array = { 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(array, false);

		heap.remove();

		assertEquals("2, 3, 6, 4, 10, 8, 7, 5, 12, 11, 19, 9, 15, 18, 14, 16, 13, 17, 20", heap.toString());
	}

	@Test
	public void heapSortHighToLow() {
		Integer[] array = { 1, 7, 5, 7, 4, 8, 3 };

		BinaryHeap.heapSort(array, false);

		Integer[] comparisonArray = { 8, 7, 7, 5, 4, 3, 1 };

		assertArrayEquals(comparisonArray, array);
	}

	@Test
	public void heapSortHighToLow2() {
		Integer[] array = { 9, 7, 14, 7, 4, 8, 3, 18, 72 };

		BinaryHeap.heapSort(array, false);

		Integer[] comparisonArray = { 72, 18, 14, 9, 8, 7, 7, 4, 3 };

		assertArrayEquals(comparisonArray, array);
	}

	@Test
	public void toArray() {
		BinaryHeap<Integer> heap = new BinaryHeap<Integer>(15, true);

		heap.insert(1);
		heap.insert(8);
		heap.insert(2);
		heap.insert(3);

		Integer[] comparison = { 8, 3, 2, 1 };
		
		assertArrayEquals(comparison, heap.toArray());
	}
}