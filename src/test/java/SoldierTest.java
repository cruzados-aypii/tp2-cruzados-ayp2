import org.junit.Test;

import logicFunctionality.Soldier;
import logicFunctionality.Auxiliary;
import logicFunctionality.BinaryHeap;
import logicFunctionality.BinaryHeapEmpty;
import logicFunctionality.Centurion;
import logicFunctionality.Legionnaire;
import logicFunctionality.Legion;

import static org.junit.Assert.*;

public class SoldierTest {

	@Test
	public void createBaseSoldiers() {
		Auxiliary auxiliary = new Auxiliary();
		Legionnaire legionnaire = new Legionnaire();
		Centurion centurion = new Centurion();

		assertEquals(100, auxiliary.getHealth(), 0);
		assertEquals(100, legionnaire.getHealth(), 0);
		assertEquals(100, centurion.getHealth(), 0);

		assertEquals(3, auxiliary.getPriority());
		assertEquals(2, legionnaire.getPriority());
		assertEquals(1, centurion.getPriority());
	}

	@Test
	public void createLegion() throws BinaryHeapEmpty {
		BinaryHeap<Soldier> soldiers = new BinaryHeap<Soldier>(10010, true);

		for (int i = 1; i <= 10; i++) {
			soldiers.insert(new Auxiliary());
		}

		for (int i = 1; i <= 10; i++) {
			soldiers.insert(new Legionnaire());
		}

		for (int i = 1; i <= 10; i++) {
			soldiers.insert(new Centurion());
		}

		Legion legion = new Legion("Roman crusaders???", soldiers);

		assertEquals("Roman crusaders???", legion.getLegionName());
		assertTrue(legion.getDefender().getPriority() == 3);
		assertEquals(30, legion.countSoldiers());
		assertTrue(legion.getSoldiers() instanceof BinaryHeap);

		for (int i = 1; i <= 10; i++) {
			soldiers.remove();
		}

		assertTrue(legion.getDefender().getPriority() == 2);
		assertEquals(20, legion.countSoldiers());

		for (int i = 1; i <= 10; i++) {
			soldiers.remove();
		}

		assertTrue(legion.getDefender().getPriority() == 1);
		assertEquals(10, legion.countSoldiers());

		for (int i = 1; i <= 10; i++) {
			soldiers.remove();
		}

		assertEquals(0, legion.countSoldiers());
		assertNull(legion.getDefender());
	}

	@Test
	public void auxiliaryAttacks() {
		Auxiliary attacker = new Auxiliary();
		Auxiliary auxiliary = new Auxiliary();

		double damageDealt = attacker.attack(auxiliary, 1.0);

		if (damageDealt != 0) {
			assertEquals(0.7, damageDealt, 0.01);
			assertEquals(99.3, auxiliary.getHealth(), 0.01);

			damageDealt = attacker.attack(auxiliary, 1.3);

			if (damageDealt != 0) {
				assertEquals(0.91, damageDealt, 0.01);
				assertEquals(98.39, auxiliary.getHealth(), 0.01);
			}

		} else {

			damageDealt = attacker.attack(auxiliary, 1.3);

			if (damageDealt != 0) {
				assertEquals(0.91, damageDealt, 0.01);
				assertEquals(99.09, auxiliary.getHealth(), 0.01);
			}
		}

		Legionnaire legionnaire = new Legionnaire();

		damageDealt = attacker.attack(legionnaire, 1.0);

		if (damageDealt != 0) {
			assertEquals(0.7, damageDealt, 0.01);
			assertEquals(99.3, legionnaire.getHealth(), 0.01);

			damageDealt = attacker.attack(legionnaire, 1.3);

			if (damageDealt != 0) {
				assertEquals(0.91, damageDealt, 0.01);
				assertEquals(98.39, legionnaire.getHealth(), 0.01);
			}

		} else {

			damageDealt = attacker.attack(legionnaire, 1.3);

			if (damageDealt != 0) {
				assertEquals(0.91, damageDealt, 0.01);
				assertEquals(99.09, legionnaire.getHealth(), 0.01);
			}
		}

		Centurion centurion = new Centurion();

		damageDealt = attacker.attack(centurion, 1.0);

		if (damageDealt != 0) {
			assertEquals(0.7, damageDealt, 0.01);
			assertEquals(99.3, centurion.getHealth(), 0.01);

			damageDealt = attacker.attack(centurion, 1.3);

			if (damageDealt != 0) {
				assertEquals(0.91, damageDealt, 0.01);
				assertEquals(98.39, centurion.getHealth(), 0.01);
			}

		} else {

			damageDealt = attacker.attack(centurion, 1.3);

			if (damageDealt != 0) {
				assertEquals(0.91, damageDealt, 0.01);
				assertEquals(99.09, centurion.getHealth(), 0.01);
			}
		}
	}

	@Test
	public void legionnaireAttacks() {
		Legionnaire attacker = new Legionnaire();
		Auxiliary auxiliary = new Auxiliary();

		double damageDealt = attacker.attack(auxiliary, 1.0);

		assertEquals(1.4, damageDealt, 0.01);
		assertEquals(98.6, auxiliary.getHealth(), 0.01);

		damageDealt = attacker.attack(auxiliary, 1.3);

		assertEquals(1.82, damageDealt, 0.01);
		assertEquals(96.78, auxiliary.getHealth(), 0.01);

		Legionnaire legionnaire = new Legionnaire();

		damageDealt = attacker.attack(legionnaire, 1.0);

		assertEquals(1.4, damageDealt, 0.01);
		assertEquals(98.6, legionnaire.getHealth(), 0.01);

		damageDealt = attacker.attack(legionnaire, 1.3);

		assertEquals(1.82, damageDealt, 0.01);
		assertEquals(96.78, legionnaire.getHealth(), 0.01);

		Centurion centurion = new Centurion();

		damageDealt = attacker.attack(centurion, 1.0);

		if (damageDealt != 0) {
			assertEquals(1.4, damageDealt, 0.01);
			assertEquals(98.6, centurion.getHealth(), 0.01);

			damageDealt = attacker.attack(centurion, 1.3);

			if (damageDealt != 0) {
				assertEquals(1.82, damageDealt, 0.01);
				assertEquals(96.78, centurion.getHealth(), 0.01);
			}

		} else {

			damageDealt = attacker.attack(centurion, 1.3);

			if (damageDealt != 0) {
				assertEquals(1.82, damageDealt, 0.01);
				assertEquals(98.18, centurion.getHealth(), 0.01);
			}
		}
	}

	@Test
	public void centurionAttacks() {
		Centurion attacker = new Centurion();
		Auxiliary auxiliary = new Auxiliary();

		double damageDealt = attacker.attack(auxiliary, 1.0);

		assertEquals(1.0, damageDealt, 0.01);
		assertEquals(99.0, auxiliary.getHealth(), 0.01);

		damageDealt = attacker.attack(auxiliary, 1.3);

		assertEquals(1.3, damageDealt, 0.01);
		assertEquals(97.70, auxiliary.getHealth(), 0.01);

		Legionnaire legionnaire = new Legionnaire();

		damageDealt = attacker.attack(legionnaire, 1.0);

		assertEquals(1.0, damageDealt, 0.01);
		assertEquals(99.0, legionnaire.getHealth(), 0.01);

		damageDealt = attacker.attack(legionnaire, 1.3);

		assertEquals(1.3, damageDealt, 0.01);
		assertEquals(97.7, legionnaire.getHealth(), 0.01);

		Centurion centurion = new Centurion();

		damageDealt = attacker.attack(centurion, 1.0);

		if (damageDealt != 0) {
			assertEquals(1.0, damageDealt, 0.01);
			assertEquals(99.0, centurion.getHealth(), 0.01);

			damageDealt = attacker.attack(centurion, 1.3);

			if (damageDealt != 0) {
				assertEquals(1.3, damageDealt, 0.01);
				assertEquals(97.7, centurion.getHealth(), 0.01);
			}

		} else {

			damageDealt = attacker.attack(centurion, 1.3);

			if (damageDealt != 0) {
				assertEquals(1.3, damageDealt, 0.01);
				assertEquals(98.7, centurion.getHealth(), 0.01);
			}
		}
	}

	@Test
	public void legionAddSoldiers() {
		Legion legion = new Legion("Not romans, but spartans");

		legion.addSoldier(new Centurion());

		assertTrue(legion.getDefender().getPriority() == 1);

		legion.addSoldier(new Centurion());

		assertTrue(legion.getDefender().getPriority() == 1);

		legion.addSoldier(new Legionnaire());

		assertTrue(legion.getDefender().getPriority() == 2);

		legion.addSoldier(new Centurion());

		assertTrue(legion.getDefender().getPriority() == 2);

		legion.addSoldier(new Legionnaire());

		assertTrue(legion.getDefender().getPriority() == 2);

		legion.addSoldier(new Auxiliary());

		assertTrue(legion.getDefender().getPriority() == 3);

		legion.addSoldier(new Auxiliary());

		assertTrue(legion.getDefender().getPriority() == 3);
	}

	@Test
	public void legionComposition() {
		Legion internal = new Legion("This are indeed romans");

		for (int i = 1; i <= 30; i++) {
			internal.addSoldier(new Legionnaire());
		}

		Legion whole = new Legion("Even more romans");

		whole.addSoldier(internal);

		assertTrue(internal.getDefender().getPriority() == 2);
		assertTrue(whole.getDefender().getPriority() == 2);
		assertEquals(internal.getDefender().getPriority(), whole.getDefender().getPriority());
		assertEquals(30, whole.countSoldiers());
		assertEquals(30, internal.countSoldiers());

		internal.addSoldier(new Auxiliary());

		assertTrue(whole.getDefender().getPriority() == 3);
		assertEquals(internal.getDefender().getPriority(), whole.getDefender().getPriority());
		assertEquals(31, whole.countSoldiers());
		assertEquals(31, internal.countSoldiers());

		assertEquals(100.0, internal.defend(100.0), 0.01); // The auxiliary dies.

		assertTrue(whole.getDefender().getPriority() == 2);
		assertEquals(internal.getDefender().getPriority(), whole.getDefender().getPriority());
		assertEquals(30, whole.countSoldiers());
		assertEquals(30, internal.countSoldiers());

		internal.addSoldier(new Centurion());

		assertFalse(whole.getDefender().getPriority() == 1);
		assertTrue(whole.getDefender().getPriority() == 2);
		assertEquals(internal.getDefender().getPriority(), whole.getDefender().getPriority());
		assertEquals(31, whole.countSoldiers());
		assertEquals(31, internal.countSoldiers());

		whole.addSoldier(new Auxiliary());

		assertTrue(whole.getDefender().getPriority() == 3); // Auxiliary precedes the legion filled with legionnaires.
		assertNotEquals(internal.getDefender().getPriority(), whole.getDefender().getPriority());
		assertEquals(32, whole.countSoldiers());
		assertEquals(31, internal.countSoldiers());

		for (int i = 1; i <= 32; i++) {
			whole.defend(100); // Kills all the soldiers in the legions.
		}

		while (whole.getDefender() != null && whole.getDefender().getPriority() == 1) {
			whole.defend(100); // Centurion might survive, this iteration makes sure it doesn't.
		}

		assertNull(whole.getDefender());
		assertNull(internal.getDefender());
		assertEquals(0, whole.countSoldiers());
		assertEquals(0, internal.countSoldiers());

		Legion composition = new Legion("Another legion");

		for (int i = 1; i <= 100; i++) {
			composition.addSoldier(new Auxiliary());
		}

		assertEquals(100, composition.countSoldiers());

		for (int i = 1; i <= 100; i++) {
			composition.addSoldier(new Legionnaire());
		}

		assertEquals(200, composition.countSoldiers());

		whole.addSoldier(composition);

		for (int i = 1; i <= 100; i++) {
			whole.addSoldier(new Auxiliary());
		}

		assertEquals(300, whole.countSoldiers());

		assertEquals(3, composition.getPriority());
		assertEquals(whole.getPriority(), composition.getPriority());

		for (int i = 1; i <= 100; i++) {
			whole.defend(100); // Kills all the auxiliaries in composition
		}

		assertEquals(2, composition.getPriority());
		assertEquals(3, whole.getPriority());
	}

	@Test
	public void legionAttacks() {
		Legion attacker = new Legion("Arabs for some reason");

		for (int i = 1; i <= 100; i++) {
			attacker.addSoldier(new Legionnaire());
		}

		Legion defender = new Legion("More arabs");

		for (int i = 1; i <= 100; i++) {
			defender.addSoldier(new Auxiliary());
		}

		assertEquals(139.2, attacker.attack(defender, 1.0), 0.01);
		assertEquals(60.8, defender.getHealth(), 0.01);
		assertEquals(99, defender.countSoldiers());

		attacker.resetLastAttackPosition();
		Auxiliary auxiliary = new Auxiliary();

		double damageDealt = attacker.attack(auxiliary, 1.0);
		attacker.resetLastAttackPosition();

		assertEquals(100.0, damageDealt, 0.01);
		assertEquals(0.0, auxiliary.getHealth(), 0.01);

		Auxiliary secondAuxiliary = new Auxiliary();

		damageDealt = attacker.attack(secondAuxiliary, 1.3);
		attacker.resetLastAttackPosition();

		assertEquals(100.0, damageDealt, 0.01);
		assertEquals(0.0, secondAuxiliary.getHealth(), 0.01);

		Legionnaire legionnaire = new Legionnaire();

		damageDealt = attacker.attack(legionnaire, 1.0);
		attacker.resetLastAttackPosition();

		assertEquals(100.0, damageDealt, 0.01);
		assertEquals(0.0, legionnaire.getHealth(), 0.01);

		Legionnaire secondLegionnaire = new Legionnaire();

		damageDealt = attacker.attack(secondLegionnaire, 1.3);
		attacker.resetLastAttackPosition();

		assertEquals(100.0, damageDealt, 0.01);
		assertEquals(0.0, secondLegionnaire.getHealth(), 0.01);

		Centurion centurion = new Centurion();

		damageDealt = attacker.attack(centurion, 1.0);
		attacker.resetLastAttackPosition();

		assertTrue(damageDealt <= 100);
		// There's a very small chance the centurion will survive ALL attacks
		if (damageDealt != 100) {
			assertTrue(centurion.getHealth() > 0 && centurion.getHealth() <= 100);
		} else {
			assertEquals(0.0, centurion.getHealth(), 0.01);
		}

		Centurion secondCenturion = new Centurion();

		damageDealt = attacker.attack(secondCenturion, 1.3);
		attacker.resetLastAttackPosition();

		assertTrue(damageDealt <= 100);
		// There's a very small chance the centurion will survive ALL attacks
		if (damageDealt != 100) {
			assertTrue(centurion.getHealth() > 0 && centurion.getHealth() <= 100);
		} else {
			assertEquals(0.0, secondCenturion.getHealth(), 0.01);
		}
	}

	@Test
	public void countEachSoldierType() {
		Legion legion = new Legion("Accountants");

		for (int i = 1; i <= 1000; i++) {
			legion.addSoldier(new Auxiliary());
		}

		for (int i = 1; i <= 1500; i++) {
			legion.addSoldier(new Legionnaire());
		}

		for (int i = 1; i <= 120; i++) {
			legion.addSoldier(new Centurion());
		}

		int[] soldierCounter = { 0, 0, 0 };

		assertEquals(2620, legion.countSoldiers(soldierCounter));

		int[] comparisonArray = { 1000, 1500, 120 };

		assertArrayEquals(comparisonArray, soldierCounter);

		Legion composition = new Legion("Lesser accountants");

		for (int i = 1; i <= 200; i++) {
			composition.addSoldier(new Auxiliary());
		}

		for (int i = 1; i <= 50; i++) {
			composition.addSoldier(new Legionnaire());
		}

		legion.addSoldier(composition);

		soldierCounter[0] = 0;
		soldierCounter[1] = 0;
		soldierCounter[2] = 0;

		assertEquals(2870, legion.countSoldiers(soldierCounter));

		comparisonArray[0] = 1200;
		comparisonArray[1] = 1550;

		assertArrayEquals(comparisonArray, soldierCounter);
	}
}
