import org.junit.Test;
import static org.junit.Assert.*;

import logicFunctionality.Army;
import logicFunctionality.Auxiliary;
import logicFunctionality.Centurion;
import logicFunctionality.Legion;
import logicFunctionality.Legionnaire;
import logicFunctionality.Player;
import logicFunctionality.RomanBattle;

public class RomanBattleTest {

	@Test
	public void throwDiceReturnsSensibleIntegers() {
		for (int i = 1; i <= 1000; i++) {
			int dice = RomanBattle.throwDice();
			assertTrue(dice <= 6 && dice >= 1);
		}
	}

	@Test
	public void createRomanBattle() {
		Player player1 = new Player("Kratos");
		Player player2 = new Player("Bayek");

		assertNotEquals(player1, player2);

		boolean player1Starts = RomanBattle.throwDice() > RomanBattle.throwDice();

		RomanBattle game = new RomanBattle(player1, player2, player1Starts);
		/*
		assertEquals(
				String.format("Auxiliaries cost 50 credits each%nLegionnaires cost 100 credits each%nCenturions"
						+ " cost 200 credits each%n%nList of predetermined legions:%n%nLegion number 1%nName:"
						+ " Generals%nTotal soldiers: 120%nTotal health: 12000%nSpecific soldiers:%n-Auxiliaries:"
						+ " 0%n-Legionnaires: 0%n-Centurions: 120%nCenturion bonus damage: 1200%%%nTotal legion"
						+ " cost: 24000 credits%nLegion's lone damage capacity: 1560.0%n%nLegion number 2%nName:"
						+ " Infantry%nTotal soldiers: 1000%nTotal health: 100000%nSpecific soldiers:%n-Auxiliaries:"
						+ " 0%n-Legionnaires: 1000%n-Centurions: 0%nCenturion bonus damage: 0%%%nTotal legion cost:"
						+ " 100000 credits%nLegion's lone damage capacity: 1400.0%n%nLegion number 3%nName:"
						+ " Fodder%nTotal soldiers: 2000%nTotal health: 200000%nSpecific soldiers:%n-Auxiliaries:"
						+ " 2000%n-Legionnaires: 0%n-Centurions: 0%nCenturion bonus damage: 0%%%nTotal legion cost:"
						+ " 100000 credits%nLegion's lone damage capacity: 1400.0%n%nLegion number 4%nName: Slave"
						+ " knights%nTotal soldiers: 4000%nTotal health: 400000%nSpecific soldiers:%n-Auxiliaries:"
						+ " 4000%n-Legionnaires: 0%n-Centurions: 0%nCenturion bonus damage: 0%%%nTotal legion cost:"
						+ " 200000 credits%nLegion's lone damage capacity: 2800.0%n%n"),
				game.listPredeterminedLegions());
				*/
		// This part of the test will fail since the predetermined legions where
		// changed, hence the list will be different when called.

		assertFalse(game.isOver());
		if (game.getCurrentPlayer() != player1) {
			assertEquals(game.getCurrentPlayer(), player2);
		} else {
			assertEquals(game.getCurrentPlayer(), player1);
		}
	}

	@Test
	public void buyLegionsAndPassTurn() {
		Player player1 = new Player("Juan Rommero");
		Player player2 = new Player("John Cena");

		assertNotEquals(player1, player2);

		RomanBattle game = new RomanBattle(player1, player2, true);

		assertEquals(game.getCurrentPlayer(), player1);

		game.buyLegion(2);

		assertEquals(1000, player1.getArmy().countSoldiers());
		assertTrue(player1.getArmy().getDefender() instanceof Legion);
		assertTrue(((Legion) player1.getArmy().getDefender()).getDefender() instanceof Legionnaire);

		game.passTurn();
		game.buyLegion(1);

		assertEquals(game.getCurrentPlayer(), player2);
		assertEquals(120, player2.getArmy().countSoldiers());
		assertTrue(player2.getArmy().getDefender() instanceof Legion);
		assertTrue(((Legion) player2.getArmy().getDefender()).getDefender() instanceof Centurion);

		game.passTurn();

		assertEquals(game.getCurrentPlayer(), player1);
	}

	@Test
	public void buySoldiers() {
		Player player1 = new Player("Diego de la Vega");
		Player player2 = new Player("Belial");

		assertNotEquals(player1, player2);

		RomanBattle game = new RomanBattle(player1, player2, false);

		assertEquals(game.getCurrentPlayer(), player2);
		assertEquals(0, player2.getArmy().countSoldiers());
		assertEquals(0, player1.getArmy().countSoldiers());
		assertEquals(500000, player2.getCredits());
		assertEquals(500000, player1.getCredits());

		game.buySoldiers(2, 2000);

		assertEquals(2000, player2.getArmy().countSoldiers());
		assertTrue(player2.getArmy().getDefender() instanceof Legionnaire);
		assertEquals(300000, player2.getCredits());

		game.buySoldiers(1, 4000);

		assertEquals(6000, player2.getArmy().countSoldiers());
		assertTrue(player2.getArmy().getDefender() instanceof Auxiliary);
		assertEquals(100000, player2.getCredits());

		game.buySoldiers(3, 500);

		assertEquals(6500, player2.getArmy().countSoldiers());
		assertTrue(player2.getArmy().getDefender() instanceof Auxiliary);
		assertEquals(0, player2.getCredits());

		game.buySoldiers(1, 500);

		assertEquals(6500, player2.getArmy().countSoldiers());
		assertTrue(player2.getArmy().getDefender() instanceof Auxiliary);
		assertEquals(0, player2.getCredits());
	}

	@Test
	public void playerAttacks() {
		Player player1 = new Player("Sancho");
		Player player2 = new Player("Berando");

		RomanBattle game = new RomanBattle(player1, player2, true);

		assertEquals(player1, game.getCurrentPlayer());
		assertEquals(0, game.playerAttacks(), 0.1);
		assertEquals(player1, game.getCurrentPlayer());

		game.buySoldiers(1, 4000);
		game.buySoldiers(3, 500);
		game.buySoldiers(2, 2000);

		assertEquals(6500, player1.getArmy().countSoldiers());

		game.passTurn();
		game.buyLegion(4);
		game.buyLegion(4);
		game.buySoldiers(3, 500);

		assertEquals(8500, player2.getArmy().countSoldiers());

		game.startGame();
		double damageDealt = game.playerAttacks();

		assertEquals(player1, game.getCurrentPlayer());
		assertTrue(damageDealt <= 311100);
		assertTrue(player1.getArmy().countSoldiers() <= 6245);
	}

	@Test
	public void specialTest() {
		Army firstArmy = new Army();
		Army secondArmy = new Army();
		Legion legion1 = new Legion("2000 legionnaires");
		Legion legion2 = new Legion("2000 legionnaires");
		Legion legion3 = new Legion("2000 legionnaires");
		Legion legion4 = new Legion("2000 legionnaires");
		Legion bigLegion1 = new Legion("4000 legionnaires");
		Legion bigLegion2 = new Legion("4000 legionnaires");

		for (int i = 1; i <= 4000; i++) {
			firstArmy.addSoldier(new Auxiliary());
		}

		for (int i = 1; i <= 500; i++) {
			firstArmy.addSoldier(new Centurion());
		}

		for (int i = 1; i <= 2000; i++) {
			firstArmy.addSoldier(new Legionnaire());
			legion1.addSoldier(new Legionnaire());
			legion2.addSoldier(new Legionnaire());
			legion3.addSoldier(new Legionnaire());
			legion4.addSoldier(new Legionnaire());
		}

		bigLegion1.addSoldier(legion1);
		bigLegion1.addSoldier(legion2);
		bigLegion2.addSoldier(legion3);
		bigLegion2.addSoldier(legion4);
		secondArmy.addSoldier(bigLegion1);
		secondArmy.addSoldier(bigLegion2);

		double damageDealt = secondArmy.attack(firstArmy);

		assertEquals(11111.2, damageDealt, 0.1);
	}
}
