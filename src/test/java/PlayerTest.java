import org.junit.Test;

import logicFunctionality.Auxiliary;
import logicFunctionality.Centurion;
import logicFunctionality.Legionnaire;
import logicFunctionality.Player;

import static org.junit.Assert.*;

public class PlayerTest {

	@Test
	public void createPlayerAndGetName() {
		Player testPlayer = new Player("Jorgito");

		assertEquals("Jorgito", testPlayer.getName());
	}

	@Test
	public void createPlayerWithNullNameAndResetNumberOfPlayers() {
		Player.resetNumberOfPlayers();

		assertEquals(0, Player.getNumberOfPlayers());

		Player nullPlayer = new Player(null);

		assertEquals(1, Player.getNumberOfPlayers());
		assertEquals("Player 1", nullPlayer.getName());

		Player testPlayer = new Player(null);

		assertEquals(2, Player.getNumberOfPlayers());
		assertEquals("Player 2", testPlayer.getName());

		Player.resetNumberOfPlayers();

		assertEquals(0, Player.getNumberOfPlayers());
		
		Player emptyPlayer = new Player("");
		
		assertEquals(1, Player.getNumberOfPlayers());
		assertEquals("Player 1", emptyPlayer.getName());
	}

	@Test
	public void playerArmyStartsEmpty() {
		Player.resetNumberOfPlayers();

		Player testPlayer = new Player("Caesar");

		assertEquals(testPlayer.getArmy().countSoldiers(), 0);
	}

	@Test
	public void playerStartsWith500kCreditsAndCantBeReducedBelow0() {
		Player.resetNumberOfPlayers();

		Player testPlayer = new Player("Augustus");

		assertEquals(500000, testPlayer.getCredits());

		testPlayer.setCredits(-1);

		assertEquals(500000, testPlayer.getCredits());

		testPlayer.setCredits(0);

		assertEquals(0, testPlayer.getCredits());
	}

	@Test
	public void toStringAndAttackRival() {
		Player.resetNumberOfPlayers();

		Player testPlayer = new Player("El Cid"); // Historically accurate Roman general

		for (int i = 1; i <= 1000; i++) {
			testPlayer.getArmy().addSoldier(new Auxiliary());
			testPlayer.getArmy().addSoldier(new Legionnaire());
		}

		for (int i = 1; i <= 100; i++) {
			testPlayer.getArmy().addSoldier(new Legionnaire());
			testPlayer.getArmy().addSoldier(new Centurion());
		}

		assertEquals(String.format(
				"Player El Cid%nTotal soldiers: 2200%nTotal health: 220000.0%nSpecific soldiers:%n-Auxiliaries: 1000%n-Legionnaires:"
						+ " 1100%n-Centurions: 100%nCenturion bonus damage: 1000%%%nTotal damage capacity: 25740.0%nCredits left: 500000"),
				testPlayer.toString());

		Player defendingPlayer = new Player("Jeanne De Arc"); // Even more historically accurate Roman general

		for (int i = 1; i <= 1000; i++) {
			defendingPlayer.getArmy().addSoldier(new Auxiliary());
			defendingPlayer.getArmy().addSoldier(new Legionnaire());
		}

		for (int i = 1; i <= 100; i++) {
			defendingPlayer.getArmy().addSoldier(new Legionnaire());
			defendingPlayer.getArmy().addSoldier(new Centurion());
		}

		assertEquals(String.format(
				"Player Jeanne De Arc%nTotal soldiers: 2200%nTotal health: 220000.0%nSpecific soldiers:%n-Auxiliaries: 1000%n-Legionnaires:"
						+ " 1100%n-Centurions: 100%nCenturion bonus damage: 1000%%%nTotal damage capacity: 25740.0%nCredits left: 500000"),
				defendingPlayer.toString());

		double damageDealt = testPlayer.attackPlayer(defendingPlayer);
		assertTrue(damageDealt <= 25740 && damageDealt >= 18040);
	}
}
