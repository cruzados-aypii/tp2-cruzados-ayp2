import org.junit.Test;
import static org.junit.Assert.*;

import logicFunctionality.Army;
import logicFunctionality.Auxiliary;
import logicFunctionality.Legionnaire;
import logicFunctionality.Centurion;
import logicFunctionality.Legion;

public class ArmyTest {

	@Test
	public void createArmyAndAddSoldiers() {
		Army army = new Army();

		assertFalse(army.hasLost());
		assertEquals(0, army.countSoldiers());
		assertEquals(1.0, army.getDamageMultiplier(), 0.01);
		assertEquals(0, army.getHealth(), 0.01);

		army.addSoldier(new Centurion());

		assertTrue(army.getDefender().getPriority() == 1);
		assertEquals(1.1, army.getDamageMultiplier(), 0.01);
		assertEquals(1, army.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);

		for (int i = 1; i <= 20; i++) {
			army.addSoldier(new Legionnaire());
		}

		assertTrue(army.getDefender().getPriority() == 2);
		assertEquals(1.1, army.getDamageMultiplier(), 0.01);
		assertEquals(21, army.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);

		for (int i = 1; i <= 500; i++) {
			army.addSoldier(new Auxiliary());
		}

		assertTrue(army.getDefender().getPriority() == 3);
		assertEquals(1.1, army.getDamageMultiplier(), 0.01);
		assertEquals(521, army.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);

		Army legionComposedArmy = new Army();

		assertFalse(legionComposedArmy.hasLost());
		assertEquals(0, legionComposedArmy.countSoldiers());
		assertEquals(1.0, legionComposedArmy.getDamageMultiplier(), 0.01);
		assertEquals(100, army.getHealth(), 0.01);

		for (int i = 1; i <= 80; i++) {
			legionComposedArmy.addSoldier(new Centurion());
		}

		assertTrue(legionComposedArmy.getDefender().getPriority() == 1);
		assertEquals(9.0, legionComposedArmy.getDamageMultiplier(), 0.01);
		assertEquals(80, legionComposedArmy.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);

		Legion auxiliaries = new Legion("Soldiers who can't land a hit properly");

		for (int i = 1; i <= 1000; i++) {
			auxiliaries.addSoldier(new Auxiliary());
		}

		legionComposedArmy.addSoldier(auxiliaries);

		assertTrue(legionComposedArmy.getDefender().getPriority() == 3);
		assertEquals(9.0, legionComposedArmy.getDamageMultiplier(), 0.01);
		assertEquals(1080, legionComposedArmy.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);

		for (int i = 1; i <= 200; i++) {
			legionComposedArmy.addSoldier(new Legionnaire());
		}

		assertTrue(legionComposedArmy.getDefender().getPriority() == 3);
		assertEquals(9.0, legionComposedArmy.getDamageMultiplier(), 0.01);
		assertEquals(1280, legionComposedArmy.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);

		Legion centurions = new Legion("OP soldiers");

		for (int i = 1; i <= 50; i++) {
			centurions.addSoldier(new Centurion());
		}

		legionComposedArmy.addSoldier(centurions);

		assertTrue(legionComposedArmy.getDefender().getPriority() == 3);
		assertEquals(14.0, legionComposedArmy.getDamageMultiplier(), 0.01);
		assertEquals(1330, legionComposedArmy.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);

		Legion composition = new Legion("Not too OP but still strong");

		for (int i = 1; i <= 25; i++) {
			composition.addSoldier(new Centurion());
		}

		legionComposedArmy.addSoldier(composition);

		assertTrue(legionComposedArmy.getDefender().getPriority() == 3);
		assertEquals(16.5, legionComposedArmy.getDamageMultiplier(), 0.01);
		assertEquals(1355, legionComposedArmy.countSoldiers());
		assertEquals(100, army.getHealth(), 0.01);
	}

	@Test
	public void armyInteractions() {
		Army armyWhichDoesntMiss = new Army();

		for (int i = 1; i <= 2000; i++) {
			armyWhichDoesntMiss.addSoldier(new Legionnaire());
		}

		assertEquals(2000, armyWhichDoesntMiss.countSoldiers());

		Army armyWhichCanMiss = new Army();

		for (int i = 1; i <= 6000; i++) {
			armyWhichCanMiss.addSoldier(new Auxiliary());
		}

		assertEquals(6000, armyWhichCanMiss.countSoldiers());

		double damageDealt = armyWhichDoesntMiss.attack(armyWhichCanMiss);

		// Because some soldiers take more damage than health they have left, some
		// damage is lost in the process of attacking. Hence, the total damage is less
		// than the army's full attack power.
		assertEquals(2778.4, damageDealt, 0.01);
		assertEquals(5973, armyWhichCanMiss.countSoldiers());
		assertEquals(21.6, armyWhichCanMiss.getHealth(), 0.01);

		damageDealt = armyWhichCanMiss.attack(armyWhichDoesntMiss);

		// Again, some soldiers take more damage than health they have left. Plus,
		// auxiliaries can miss their attacks, meaning it is possible for all 5973
		// auxiliaries to deal absolutely no damage.
		assertTrue(damageDealt <= 5973 * 0.7 && damageDealt >= 0);
		assertTrue(armyWhichDoesntMiss.countSoldiers() <= 2000 && armyWhichDoesntMiss.countSoldiers() >= 1959);

		Army damageBoostedArmy = new Army();

		for (int i = 1; i <= 100; i++) {
			damageBoostedArmy.addSoldier(new Centurion());
		}

		for (int i = 1; i <= 800; i++) {
			damageBoostedArmy.addSoldier(new Legionnaire());
		}

		assertEquals(900, damageBoostedArmy.countSoldiers());
		assertEquals(11.0, damageBoostedArmy.getDamageMultiplier(), 0.01);

		Army defendingArmy = new Army();

		for (int i = 1; i <= 4000; i++) {
			defendingArmy.addSoldier(new Legionnaire());
		}

		assertEquals(4000, defendingArmy.countSoldiers());

		damageDealt = damageBoostedArmy.attack(defendingArmy);

		assertEquals(12383.6, damageDealt, 0.01);
		assertEquals(3877, defendingArmy.countSoldiers());

		Army legionComposedArmy = new Army();
		Legion legion = new Legion("Composition");
		Legion legion2 = new Legion("Second composition");

		for (int i = 1; i <= 1000; i++) {
			legion.addSoldier(new Legionnaire());
		}

		for (int i = 1; i <= 1000; i++) {
			legion2.addSoldier(new Legionnaire());
		}

		legionComposedArmy.addSoldier(legion);
		legionComposedArmy.addSoldier(legion2);

		assertEquals(2000, legionComposedArmy.countSoldiers());

		damageDealt = legionComposedArmy.attack(defendingArmy);

		assertEquals(2778, damageDealt, 0.1);
		assertEquals(3849, defendingArmy.countSoldiers());

		damageDealt = damageBoostedArmy.attack(legionComposedArmy);

		assertEquals(12383.6, damageDealt, 0.01);
		assertEquals(1877, legionComposedArmy.countSoldiers());

		legionComposedArmy = new Army();
		legion = new Legion("Composition");
		legion2 = new Legion("Second composition");
		Army defendingLegionArmy = new Army();
		Legion defendingLegion = new Legion("Third composition");

		for (int i = 1; i <= 1000; i++) {
			legion.addSoldier(new Legionnaire());
			legion2.addSoldier(new Legionnaire());
			defendingLegion.addSoldier(new Legionnaire());
		}

		legionComposedArmy.addSoldier(legion);
		legionComposedArmy.addSoldier(legion2);
		legionComposedArmy.addSoldier(new Centurion());
		defendingLegionArmy.addSoldier(defendingLegion);

		assertEquals(2001, legionComposedArmy.countSoldiers());
		assertEquals(1000, defendingLegionArmy.countSoldiers());

		damageDealt = legionComposedArmy.attack(defendingLegionArmy);

		assertEquals(3078.1, damageDealt, 0.1);
		assertEquals(970, defendingLegionArmy.countSoldiers());
	}

	@Test
	public void destroyAnArmy() {
		Army destroyers = new Army();

		for (int i = 1; i <= 9999; i++) {
			destroyers.addSoldier(new Centurion());
		}

		assertEquals(9999, destroyers.countSoldiers());
		assertEquals(1000.9, destroyers.getDamageMultiplier(), 0.01);

		Army soonToBeDestroyedArmy = new Army();

		for (int i = 1; i <= 2000; i++) {
			soonToBeDestroyedArmy.addSoldier(new Legionnaire());
		}

		assertEquals(2000, soonToBeDestroyedArmy.countSoldiers());

		double damageDealt = destroyers.attack(soonToBeDestroyedArmy);

		// Fun fact: the attacking army's full attack power is of 10 007 999.1, but the
		// defending army only has 200 000 health points.
		assertEquals(damageDealt, 200000, 0.01);
		assertEquals(0, soonToBeDestroyedArmy.countSoldiers());
		assertTrue(soonToBeDestroyedArmy.hasLost());
	}

	@Test
	public void losingCenturionsReducesDamageMultiplier() {
		Army centurions = new Army();

		for (int i = 0; i <= 100; i++) {
			centurions.addSoldier(new Centurion());
		}

		assertEquals(11.0, centurions.getDamageMultiplier(), 0.1);

		Army attackers = new Army();

		for (int i = 1; i <= 1000; i++) {
			attackers.addSoldier(new Legionnaire());
		}

		while (centurions.countSoldiers() > 90) {
			attackers.attack(centurions);
		}

		assertTrue(centurions.getDamageMultiplier() <= 10.0);

		Army moreCenturions = new Army();

		Legion legion = new Legion("More centurions");

		for (int i = 1; i <= 100; i++) {
			legion.addSoldier(new Centurion());
		}

		moreCenturions.addSoldier(legion);

		assertEquals(11.0, moreCenturions.getDamageMultiplier(), 0.1);

		attackers.attack(moreCenturions);

		while (moreCenturions.countSoldiers() > 90) {
			attackers.attack(moreCenturions);
		}

		assertEquals((moreCenturions.getDamageMultiplier() - 1) * 10, moreCenturions.countSoldiers(), 0.1);
	}

	@Test
	public void countEachSoldierType() {
		Army army = new Army();

		for (int i = 1; i <= 1000; i++) {
			army.addSoldier(new Auxiliary());
		}

		for (int i = 1; i <= 1500; i++) {
			army.addSoldier(new Legionnaire());
		}

		for (int i = 1; i <= 120; i++) {
			army.addSoldier(new Centurion());
		}

		int[] soldierCounter = { 0, 0, 0 };

		assertEquals(2620, army.countSoldiers(soldierCounter));

		int[] comparisonArray = { 1000, 1500, 120 };

		assertArrayEquals(comparisonArray, soldierCounter);

		Legion composition = new Legion("Some accountants");

		for (int i = 1; i <= 200; i++) {
			composition.addSoldier(new Auxiliary());
		}

		for (int i = 1; i <= 50; i++) {
			composition.addSoldier(new Legionnaire());
		}

		army.addSoldier(composition);

		soldierCounter[0] = 0;
		soldierCounter[1] = 0;
		soldierCounter[2] = 0;

		assertEquals(2870, army.countSoldiers(soldierCounter));

		comparisonArray[0] = 1200;
		comparisonArray[1] = 1550;

		assertArrayEquals(comparisonArray, soldierCounter);
	}
}
