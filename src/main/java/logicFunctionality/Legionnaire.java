package logicFunctionality;

import java.math.BigDecimal;

public class Legionnaire extends Soldier {

	/**
	 * Creates a new Legionnaire type Soldier. Starts with 100 health points and has
	 * priority over Centurion types to be attacked, but not over Auxiliary types.
	 */
	public Legionnaire() {
		super(2);
	}

	@Override
	/**
	 * Attacks the indicated Soldier and returns the damage dealt. Legionnaire has
	 * 1.4 base damage and possesses no special attacking abilities other than
	 * having the highest damage output. Returns 0 if this Soldier is dead.
	 */
	public double attack(Soldier defender, double multiplier) {
		if (this.health.doubleValue() > 0) {
			return defender.defend(new BigDecimal("1.4").multiply(new BigDecimal("" + multiplier)).doubleValue());
		} else {
			return 0;
		}
	}

	@Override
	/**
	 * Receives incoming damage and returns the damage taken. Once at least 100
	 * damage is taken (not received), the Soldier dies and can't attack or defend
	 * again. Legionnaire has no special defending abilities. Returns 0 if this
	 * Soldier is already dead.
	 */
	public double defend(double incomingDamage) {
		double damageReceived = 0;

		if (this.health.subtract(new BigDecimal("" + incomingDamage)).doubleValue() < 0) {
			damageReceived = this.health.doubleValue();
			this.health = new BigDecimal("0");
		} else {
			damageReceived = incomingDamage;
			this.health = this.health.subtract(new BigDecimal("" + incomingDamage));
		}

		return damageReceived;
	}

}
