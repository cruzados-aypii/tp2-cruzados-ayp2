package logicFunctionality;

import java.math.BigDecimal;

public abstract class Soldier implements Comparable<Soldier> {

	protected BigDecimal health;
	protected int priority;

	/**
	 * Super constructor implemented by child classes. Creates an instance of a
	 * Soldier-child class with the indicated priority to be attacked.
	 * 
	 * @param priority
	 */
	protected Soldier(int priority) {
		this.health = new BigDecimal("100");
		this.priority = priority;
	}

	abstract public double attack(Soldier defender, double multiplier);

	abstract public double defend(double incomingDamage);

	/**
	 * Returns this Soldier's current health points.
	 * 
	 * @return
	 */
	public double getHealth() {
		return this.health.doubleValue();
	}

	/**
	 * Returns this Soldier's priority to be attacked.
	 * 
	 * @return
	 */
	public int getPriority() {
		return this.priority;
	}

	@Override
	/**
	 * Compares this Soldier to another Soldier. Returns a positive number if other
	 * has less priority, negative if other has more priority and 0 if both Soldiers
	 * have the same priority.
	 */
	public int compareTo(Soldier other) {
		return this.priority - other.getPriority();
	}
}
