package logicFunctionality;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileConverter {

	public static void convertFC(String filePath) throws IOException {
		File file = new File(filePath);
		String oldContent = "";
		BufferedReader br = new BufferedReader(
				new FileReader(file));
		String line = br.readLine();

		while (line != null) {
			oldContent += line + System.lineSeparator();
			line = br.readLine();
		}
		
		String newContent = oldContent.replaceAll(";", ",");
		
		FileWriter fw = new FileWriter(file);
		
		fw.write(newContent);
		
		br.close();
		fw.close();
	}
	
	public static void convertFPC(String filePath) throws IOException {
		File file = new File(filePath);
		String oldContent = "";
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = br.readLine();

		while (line != null) {
			oldContent += line + System.lineSeparator();
			line = br.readLine();
		}
		
		String newContent = oldContent.replaceAll(",", ";");
		
		FileWriter fw = new FileWriter(file);
		
		fw.write(newContent);
		
		br.close();
		fw.close();
	}
}
