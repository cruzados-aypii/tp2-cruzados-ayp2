package logicFunctionality;

public class BinaryHeapEmpty extends Exception {

	private static final long serialVersionUID = -9104381189492953640L;

	public BinaryHeapEmpty() {
		super("The heap is empty.");
	}
}
