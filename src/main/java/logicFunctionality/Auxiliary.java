package logicFunctionality;

import java.math.BigDecimal;

public class Auxiliary extends Soldier {

	/**
	 * Creates a new Auxiliary type Soldier. Starts with 100 health points, and has
	 * priority to be attacked over other Soldier types.
	 */
	public Auxiliary() {
		super(3);
	}

	@Override
	/**
	 * Attacks the indicated soldier applying the given multiplier and returns the
	 * damage dealt. Auxiliary has 0.7 base damage and misses its attacks 50% of the
	 * time. Returns 0 if this Soldier is dead.
	 */
	public double attack(Soldier defender, double multiplier) {
		double damageDealt = 0;
		double temp = Math.random() * 100;

		if (temp > 50) {
			damageDealt = defender
					.defend(new BigDecimal("0.7").multiply(new BigDecimal("" + multiplier)).doubleValue());
		} else {
			damageDealt = 0;
		}

		return damageDealt;
	}

	@Override
	/**
	 * Receives the given damage and returns the damage taken. Once at least 100
	 * damage is taken (not received), the Soldier dies and can't attack or defend
	 * again. Auxiliary has no special abilities that allow it to defend different
	 * from other Soldier types. Returns 0 if already dead.
	 */
	public double defend(double incomingDamage) {
		if (this.health.doubleValue() > 0) {
			double damageReceived = 0;

			if (this.health.subtract(new BigDecimal("" + incomingDamage)).doubleValue() < 0) {
				damageReceived = this.health.doubleValue();
				this.health = new BigDecimal("0");
			} else {
				damageReceived = incomingDamage;
				this.health = this.health.subtract(new BigDecimal("" + incomingDamage));
			}

			return damageReceived;
		} else {
			return 0;
		}
	}

}
