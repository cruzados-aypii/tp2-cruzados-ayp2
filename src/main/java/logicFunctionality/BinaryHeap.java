package logicFunctionality;

@SuppressWarnings("unchecked")
public class BinaryHeap<T extends Comparable<? super T>> {

	private static final int DEFAULT_CAPACITY = 100;
	private int lastPosition;
	private T[] array;
	private boolean isMax;

	// Constructors
	/**
	 * Creates a BinaryHeap containing the elements of the given array. isMax
	 * indicates if it's a max or minimum heap. The array can't contain any null
	 * references. The heap's capacity expands dynamically when necessary.
	 * 
	 * @param array
	 * @param isMax
	 * @throws NullPointerException
	 */
	public BinaryHeap(T[] array, boolean isMax) throws NullPointerException {

		for (int i = 0; i < array.length; i++) {
			if (array[i] == null) {
				throw new NullPointerException();
			}
		}

		this.isMax = isMax;
		heapify(array, this.isMax);
		this.array = array;
		this.lastPosition = array.length - 1;
	}

	/**
	 * Creates an empty BinaryHeap with the indicated capacity. isMax indicates if
	 * it's a max or minimum heap. The heap's capacity expands dynamically when
	 * necessary.
	 * 
	 * @param capacity
	 * @param isMax
	 */
	public BinaryHeap(int capacity, boolean isMax) {
		this.array = (T[]) (new Comparable[capacity]);
		this.lastPosition = -1;
		this.isMax = isMax;
	}

	/**
	 * Creates an empty default minimum BinaryHeap with capacity for 10 elements.
	 * It's capacity expands dynamically when necessary.
	 */
	public BinaryHeap() {
		this(DEFAULT_CAPACITY, false);
	}

	/**
	 * Creates an empty BinaryHeap with default capacity for 10 elements. isMax
	 * indicates if it's a max or minimum heap. The heap's capacity expands
	 * dynamically when necessary.
	 * 
	 * @param isMax
	 */
	public BinaryHeap(boolean isMax) {
		this(DEFAULT_CAPACITY, isMax);
	}

	// Public operations
	/**
	 * Prints the contents of the heap using each element's toString() method.
	 */
	public void print() {

		System.out.println("Printing heap with " + (lastPosition + 1) + " elements");

		for (int i = 0; i <= lastPosition; i++) {
			System.out.println(array[i]);
		}

		System.out.println("\n");
	}

	/**
	 * Returns a String containing each element's toString() description separated
	 * by commas.
	 */
	public String toString() {
		String list = "";

		try {
			if (isEmpty()) {
				throw new BinaryHeapEmpty();
			}

			list = array[0].toString();

			for (int i = 1; i <= lastPosition; i++) {
				list += ", " + array[i].toString();
			}
		} catch (BinaryHeapEmpty e) {
			list = "Heap is empty";
		}

		return list;
	}

	/**
	 * Inserts the indicated node in the heap. The node can't be null.
	 * 
	 * @param node
	 * @throws NullPointerException
	 */
	public void insert(T node) throws NullPointerException {

		if (node == null) {
			throw new NullPointerException();
		}

		if (lastPosition + 1 >= array.length) {
			T[] newArray = (T[]) (new Comparable[array.length * 2]);
			for (int i = 0; i < array.length; i++) {
				newArray[i] = array[i];
			}
			array = newArray;
		}

		lastPosition++;
		array[lastPosition] = node;

		upheap(lastPosition);
	}

	/**
	 * Returns the node at the top of the heap.
	 * 
	 * @return
	 * @throws BinaryHeapEmpty
	 */
	public T peek() throws BinaryHeapEmpty {
		if (isEmpty()) {
			throw new BinaryHeapEmpty();
		}

		return array[0];
	}

	/**
	 * Returns the node at the top of the heap and removes it from the structure.
	 * 
	 * @return
	 * @throws BinaryHeapEmpty
	 */
	public T remove() throws BinaryHeapEmpty {

		if (isEmpty()) {
			throw new BinaryHeapEmpty();
		}

		T root = array[0];
		swap(0, lastPosition, array);
		lastPosition--;

		downheap(0, array, lastPosition, isMax);
		return root;
	}

	/**
	 * Empties the heap. Be careful with this method.
	 */
	public void makeEmpty() {
		lastPosition = -1;
	}

	/**
	 * Orders the given array using the heapsort algorithm. lowToHigh indicates the
	 * desired order.
	 * 
	 * @param array
	 * @param lowToHigh
	 */
	public static <I extends Comparable<? super I>> void heapSort(I[] array, boolean lowToHigh) {
		if (array.length > 0) {

			heapify(array, lowToHigh);

			int i = array.length - 1;
			while (i > 0) {
				swap(0, i, array);
				downheap(0, array, --i, lowToHigh);
			}
		}
	}

	/**
	 * Reorders the given array in a way it represents a BinaryHeap. isMax indicates
	 * whether the array should be reordered as max or minimum heap.
	 * 
	 * @param array
	 * @param isMax
	 */
	public static <I extends Comparable<? super I>> void heapify(I[] array, boolean isMax) {
		int lastNonLeafPosition = getLastNonLeafPosition(array);

		while (lastNonLeafPosition >= 0) {
			downheap(lastNonLeafPosition, array, array.length - 1, isMax);
			lastNonLeafPosition--;
		}
	}

	/**
	 * Custom method added for implementation in RomanBattle. "Downheaps" the legion
	 * on the top.
	 */
	public void updateTop() {
		downheap(0, this.array, this.lastPosition, this.isMax);
	}

	/**
	 * Returns the last node of the heap.
	 * 
	 * @return
	 */
	public T getLast() {
		return array[lastPosition];
	}

	/**
	 * Returns true if the heap is empty.
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return lastPosition < 0;
	}

	/**
	 * Returns a heapified array representing the heap.
	 * 
	 * @return
	 */
	public T[] toArray() {
		T[] newArray = (T[]) (new Comparable[lastPosition + 1]);
		for (int i = 0; i <= lastPosition; i++) {
			newArray[i] = array[i];
		}
		return newArray;
	}

	/**
	 * Returns the internal array used to implement the heap. This method should
	 * only be used to read; modification of the returned array will cause the heap
	 * to behave erratically. Use getLastPosition() instead of array.length to
	 * iterate the array. Note that this method differs from toArray(), as the
	 * latter will return a new array containing only the relevant elements.
	 * 
	 * @return
	 */
	public T[] getArray() {
		return this.array;
	}

	/**
	 * Returns the last relevant position of the array returned by getArray().
	 * 
	 * @return
	 */
	public int getLastPosition() {
		return lastPosition;
	}

	// Private operations
	/**
	 * Internal operation, drags a node upwards in the heap.
	 * 
	 * @param position
	 */
	private void upheap(int position) {

		for (; position > 0
				&& compare(array[position], array[(position - 1) / 2], isMax); position = (position - 1) / 2) {
			swap(position, (position - 1) / 2, array);
		}
	}

	/**
	 * Internal operation, drags a node downwards in the heap.
	 * 
	 * @param position
	 * @param array
	 * @param lastPosition
	 * @param isMax
	 */
	private static <I extends Comparable<? super I>> void downheap(int position, I[] array, int lastPosition,
			boolean isMax) {

		int child;

		for (; (position * 2) + 1 <= lastPosition; position = child) {
			child = (position * 2) + 1;

			if (child != lastPosition && compare(array[child + 1], array[child], isMax)) {
				child++;
			}

			if (compare(array[child], array[position], isMax)) {
				swap(position, child, array);
			} else {
				break;
			}
		}
	}

	/**
	 * Internal operation, returns the last non-leaf/parent node of the heap.
	 * 
	 * @param array
	 * @return
	 */
	private static <I extends Comparable<? super I>> int getLastNonLeafPosition(I[] array) {
		return (array.length - 2) / 2;
	}

	/**
	 * Internal operation, compares two elements according to the heap's criteria.
	 * isMax defines the criteria, indicating if it's either a max or minimum heap
	 * environment.
	 * 
	 * @param x
	 * @param y
	 * @param isMax
	 * @return
	 */
	private static <I extends Comparable<? super I>> boolean compare(I x, I y, boolean isMax) {
		if (isMax) {
			return x.compareTo(y) > 0;
		} else {
			return x.compareTo(y) < 0;
		}
	}

	/**
	 * Internal operation, swaps positions between two elements of the given array.
	 * 
	 * @param x
	 * @param y
	 * @param array
	 */
	private static <I extends Comparable<? super I>> void swap(int x, int y, I[] array) {
		I temp = array[x];
		array[x] = array[y];
		array[y] = temp;
	}
}
