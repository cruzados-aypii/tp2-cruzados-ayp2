package logicFunctionality;

import java.math.BigDecimal;

public class Legion extends Soldier {

	private BinaryHeap<Soldier> soldiers;
	private String legionName;

	private int lastAttackPosition;

	/**
	 * Creates an empty Legion with the indicated legion name.
	 * 
	 * @param name
	 */
	public Legion(String name) {
		super(0);
		this.soldiers = new BinaryHeap<Soldier>(2000, true);
		this.legionName = name;
		this.lastAttackPosition = -1;
	}

	/**
	 * Creates a Legion with indicated legion name and the specified soldiers.
	 * 
	 * @param name
	 * @param soldiers
	 */
	public Legion(String name, BinaryHeap<Soldier> soldiers) {
		super(0);
		this.soldiers = soldiers;
		this.legionName = name;
		this.lastAttackPosition = -1;

		this.priority = getDefender().getPriority();
	}

	@Override
	/**
	 * Attacks the indicated Soldier and returns the damage dealt. A Legion may
	 * attack several Soldiers in succession. Returns 0 if there are no soldiers
	 * alive in the Legion.
	 */
	public double attack(Soldier defender, double multiplier) {
		if (getHealth() > 0) {
			Comparable<Soldier>[] attackers = soldiers.getArray();
			BigDecimal damageDealt = new BigDecimal("0");
			Legion lastLegion = null;
			if (this.lastAttackPosition == -1) {
				this.lastAttackPosition = 0;
			}

			while (lastAttackPosition <= soldiers.getLastPosition() && defender.getHealth() > 0) {
				damageDealt = damageDealt.add(
						new BigDecimal("" + ((Soldier) attackers[lastAttackPosition]).attack(defender, multiplier)));
				if (!(attackers[lastAttackPosition] instanceof Legion)) {
					lastAttackPosition++;
				} else {
					lastLegion = ((Legion) attackers[lastAttackPosition]);
					if (lastLegion.getLastAttackPosition() == -1) {
						lastAttackPosition++;
					}
				}

			}

			if (lastAttackPosition > soldiers.getLastPosition()) {
				resetLastAttackPosition();
			}

			return damageDealt.doubleValue();
		} else {
			return 0;
		}
	}

	@Override
	/**
	 * Receives incoming damage and returns damage taken. The Soldier with the
	 * highest priority to be attacked in the Legion defends. Returns 0 if no
	 * Soldiers are alive.
	 */
	public double defend(double incomingDamage) {
		if (getHealth() > 0) {
			double damageReceived = 0;

			try {
				damageReceived = getDefender().defend(incomingDamage);

				if (getDefender() instanceof Legion) {
					soldiers.updateTop();
				}

				if (getHealth() <= 0) {

					soldiers.remove();

					if (getDefender() != null) {
						this.priority = getDefender().getPriority();
					} else {
						priority = 0;
					}

				} else if (getDefender() instanceof Legion) {
					soldiers.updateTop();
				}
			} catch (BinaryHeapEmpty e) {
				System.out.println("No soldiers remain.");
			}

			return damageReceived;
		} else {
			return 0;
		}
	}

	/**
	 * Returns the Soldier with the highest priority in the Legion. If this method
	 * returns a Legion, it means that Legion's top-priority Soldier will defend.
	 * 
	 * @return
	 */
	public Soldier getDefender() {
		try {
			return soldiers.peek();
		} catch (BinaryHeapEmpty e) {
			return null;
		}
	}

	/**
	 * Returns the health points of the Soldier with the highest priority in the
	 * Legion. This method doesn't return Legion objects, but may return a Soldier
	 * contained in a sub-legion.
	 */
	public double getHealth() {
		if (getDefender() != null) {
			return getDefender().getHealth();
		} else {
			return new BigDecimal("0").doubleValue();
		}
	}

	/**
	 * Adds the given Soldier to the Legion. If the Soldier is dead or null, it
	 * won't be added.
	 * 
	 * @param newSoldier
	 */
	public void addSoldier(Soldier newSoldier) {
		if (newSoldier != null && newSoldier.getHealth() > 0) {
			try {
				soldiers.insert(newSoldier);
				this.priority = soldiers.peek().getPriority();
			} catch (BinaryHeapEmpty e) {
				System.out.println(e);
			}
		}
	}

	/**
	 * Returns this Legion's name.
	 * 
	 * @return
	 */
	public String getLegionName() {
		return this.legionName;
	}

	/**
	 * Returns the internal BinaryHeap which contains the Legion's Soldiers. Use
	 * only to read, if modified, the Legion could behave erratically.
	 * 
	 * @return
	 */
	public BinaryHeap<Soldier> getSoldiers() {
		return this.soldiers;
	}

	/**
	 * Returns the total Soldiers contained in the Legion.
	 * 
	 * @return
	 */
	public int countSoldiers() {
		int totalSoldiers = 0;
		Comparable<Soldier>[] soldierArray = soldiers.getArray();

		for (int i = 0; i <= soldiers.getLastPosition(); i++) {
			if (soldierArray[i] instanceof Legion) {
				totalSoldiers += ((Legion) soldierArray[i]).countSoldiers();
			} else {
				totalSoldiers++;
			}
		}

		return totalSoldiers;
	}

	/**
	 * Returns the total Soldiers contained in the Legion and saves the amount of
	 * Soldiers of each type in the given array. Auxiliaries in position 0,
	 * Legionnaires in position 1 and Centurions in position 2. soldierCounter's
	 * first three positions should be initialized at 0 to effectively count each
	 * Soldier type.
	 * 
	 * @param soldierCounter
	 * @return
	 */
	public int countSoldiers(int[] soldierCounter) {
		int totalSoldiers = 0;
		Comparable<Soldier>[] soldierArray = soldiers.getArray();

		for (int i = 0; i <= soldiers.getLastPosition(); i++) {
			Soldier soldier = (Soldier) soldierArray[i];
			if (soldier instanceof Legion) {
				totalSoldiers += ((Legion) soldier).countSoldiers(soldierCounter);
			} else if (soldier.getPriority() == 3) {
				soldierCounter[0]++;
				totalSoldiers++;
			} else if (soldier.getPriority() == 2) {
				soldierCounter[1]++;
				totalSoldiers++;
			} else {
				soldierCounter[2]++;
				totalSoldiers++;
			}
		}
		return totalSoldiers;
	}

	/**
	 * Returns the damage bonus from Centurion Soldiers.
	 * 
	 * @return
	 */
	public double calculateDamageBonus() {
		BigDecimal damageBonus = new BigDecimal("0");
		Comparable<Soldier>[] soldierArray = soldiers.getArray();

		for (int i = 0; i <= soldiers.getLastPosition(); i++) {
			if (soldierArray[i] instanceof Legion) {
				damageBonus = damageBonus.add(new BigDecimal("" + ((Legion) soldierArray[i]).calculateDamageBonus()));
			} else if (((Soldier) soldierArray[i]).getPriority() == 1) {
				damageBonus = damageBonus.add(new BigDecimal("0.1"));
			}
		}

		return damageBonus.doubleValue();
	}

	/**
	 * Returns a clone of this Legion, which is a separate object from this.
	 */
	public Legion clone() {
		BinaryHeap<Soldier> cloneSoldiers = new BinaryHeap<Soldier>(soldiers.getLastPosition() + 1, true);

		Comparable<Soldier>[] soldierArray = soldiers.getArray();

		for (int i = 0; i <= soldiers.getLastPosition(); i++) {
			if (soldierArray[i] instanceof Legion) {
				cloneSoldiers.insert(((Legion) soldierArray[i]).clone());
			} else if (((Soldier) soldierArray[i]).getPriority() == 3) {
				cloneSoldiers.insert(new Auxiliary());
			} else if (((Soldier) soldierArray[i]).getPriority() == 2) {
				cloneSoldiers.insert(new Legionnaire());
			} else if (((Soldier) soldierArray[i]).getPriority() == 1) {
				cloneSoldiers.insert(new Centurion());
			}

		}

		Legion clone = new Legion(this.legionName, cloneSoldiers);
		return clone;
	}

	/**
	 * Returns the last position which can attack. Returns -1 if all or none
	 * positions attacked.
	 * 
	 * @return
	 */
	public int getLastAttackPosition() {
		return this.lastAttackPosition;
	}

	/**
	 * Resets the counter of attacks. This method is meant for implementation in
	 * RomanBattle and should only be called after the Legion is done attacking; for
	 * example, after successfully defeating another Army without using all
	 * Soldiers, this method should be called so the next time the Legion attacks,
	 * the few remaining Soldiers won't be the only ones to attack.
	 */
	public void resetLastAttackPosition() {
		this.lastAttackPosition = -1;

		Comparable<Soldier>[] soldierArray = soldiers.getArray();

		for (int i = 0; i <= soldiers.getLastPosition(); i++) {
			if (soldierArray[i] instanceof Legion) {
				((Legion) soldierArray[i]).resetLastAttackPosition();
			}
		}
	}
}
