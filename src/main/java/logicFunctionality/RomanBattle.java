package logicFunctionality;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.StringTokenizer;

public class RomanBattle {

	private Player player1;
	private Player player2;
	private Player currentPlayer;
	private Legion[] predeterminedLegions;
	private int[] legionCosts;
	private boolean gameStarted;

	/**
	 * Creates a new instance of RomanBattle with the indicated players.
	 * player1Starts indicates whether player 1 or player 2 will start.
	 * 
	 * @param player1
	 * @param player2
	 * @param player1Starts
	 */
	public RomanBattle(Player player1, Player player2, boolean player1Starts) {
		this.player1 = player1;
		this.player2 = player2;
		if (player1Starts) {
			this.currentPlayer = player1;
		} else {
			this.currentPlayer = player2;
		}

		this.gameStarted = false;
		this.getPrediterminedLegions();
	}

	/**
	 * Makes getCurrentPlayer() Army attack the opponent's Army. Returns the damage
	 * dealt. Returns 0 if the game hasn't started.
	 * 
	 * @return
	 */
	public double playerAttacks() {
		if (gameStarted) {
			Player attacker = currentPlayer;
			passTurn();
			return attacker.attackPlayer(currentPlayer);
		} else
			return 0;
	}

	/**
	 * Makes getCurrentPlayer() buy the specified predetermined Legion.
	 * listPredeterminedLegions() lists the offered Legions and their respective
	 * number identification. legionNumber indicates which Legion the Player is
	 * buying. Returns the Player's new balance. No Legion will be added to the Army
	 * if the Player doesn't have enough credits. Call getCredits() from the Player
	 * instance to check remaining credits.
	 * 
	 * @param legionNumber
	 * @return
	 */
	public int buyLegion(int legionNumber) {

		if (currentPlayer.getCredits() - legionCosts[legionNumber - 1] >= 0) {
			currentPlayer.getArmy().addSoldier(predeterminedLegions[legionNumber - 1].clone());
			currentPlayer.setCredits(currentPlayer.getCredits() - legionCosts[legionNumber - 1]);
		}

		return currentPlayer.getCredits();
	}

	/**
	 * Buys the specified amount of the indicated type of Soldier for
	 * getCurrentPlayer(). No Soldier will be bought if the Player doesn't have
	 * enough credits for the entire purchase. Call getCredits() from the Player
	 * instance to check remaining credits. Soldier type number identification:
	 * -Auxiliary: 1 -Legionnaire: 2 -Centurion: 3
	 * 
	 * @param typeOfSoldier
	 * @param quantity
	 * @return
	 */
	public int buySoldiers(int typeOfSoldier, int quantity) {
		if (quantity > 0) {
			switch (typeOfSoldier) {

			case 1:
				if (currentPlayer.getCredits() - (50 * quantity) >= 0) {
					for (int i = 1; i <= quantity; i++) {
						currentPlayer.getArmy().addSoldier(new Auxiliary());
					}
					currentPlayer.setCredits(currentPlayer.getCredits() - (50 * quantity));
				}
				break;
			case 2:
				if (currentPlayer.getCredits() - (100 * quantity) >= 0) {
					for (int i = 1; i <= quantity; i++) {
						currentPlayer.getArmy().addSoldier(new Legionnaire());
					}
					currentPlayer.setCredits(currentPlayer.getCredits() - (100 * quantity));
				}
				break;
			case 3:
				if (currentPlayer.getCredits() - (200 * quantity) >= 0) {
					for (int i = 1; i <= quantity; i++) {
						currentPlayer.getArmy().addSoldier(new Centurion());
					}
					currentPlayer.setCredits(currentPlayer.getCredits() - (200 * quantity));
				}
				break;
			}
		}

		return currentPlayer.getCredits();
	}

	/**
	 * Lists the predetermined legions in offer.
	 * 
	 * @return
	 */
	public String listPredeterminedLegions() { // This method isn't tested because the contents of the .txt file may be
												// changed, thus changing the output of this method.
		String list = String.format(
				"Auxiliaries cost 50 credits each%nLegionnaires cost 100 credits each%nCenturions cost 200 credits"
						+ " each%n%nList of predetermined legions:%n%n");

		for (int i = 0; i < predeterminedLegions.length; i++) {
			int[] soldierCounter = { 0, 0, 0 };
			// First position auxiliaries, second position legionnaires, third position
			// centurions

			list += String.format("Legion number " + (i + 1) + "%nName: " + predeterminedLegions[i].getLegionName()
					+ "%nTotal soldiers: " + predeterminedLegions[i].countSoldiers(soldierCounter) + "%nTotal health: "
					+ (100 * predeterminedLegions[i].countSoldiers()) + "%nSpecific soldiers:%n-Auxiliaries: "
					+ soldierCounter[0] + "%n-Legionnaires: " + soldierCounter[1] + "%n-Centurions: "
					+ soldierCounter[2] + "%nCenturion bonus damage: "
					+ ((int) (predeterminedLegions[i].calculateDamageBonus()) * 100) + "%%");

			int totalCost = 0;

			totalCost += soldierCounter[0] * 50;
			totalCost += soldierCounter[1] * 100;
			totalCost += soldierCounter[2] * 200;

			list += String.format("%nTotal legion cost: " + totalCost + " credits");

			BigDecimal totalDamage;
			BigDecimal multiplier = new BigDecimal(predeterminedLegions[i].calculateDamageBonus() + 1);

			totalDamage = new BigDecimal("" + soldierCounter[0]).multiply(new BigDecimal("0.7"));
			totalDamage = (new BigDecimal("" + soldierCounter[1]).multiply(new BigDecimal("1.4"))).add(totalDamage);
			totalDamage = (new BigDecimal("" + soldierCounter[2]).multiply(new BigDecimal("1"))).add(totalDamage);
			totalDamage = totalDamage.multiply(multiplier);

			list += String.format("%nLegion's lone damage capacity: " + totalDamage + "%n%n");
		}

		return list;
	}

	/**
	 * Static method which mimics a six-faced dice. Implemented to decide which
	 * player goes first.
	 * 
	 * @return
	 */
	public static int throwDice() {
		return (int) (Math.random() * 6 + 1);
	}

	/**
	 * Passes the turn to the other Player. This changes getCurrentPlayer() return
	 * value.
	 */
	public void passTurn() {
		if (currentPlayer == player1) {
			currentPlayer = player2;
		} else {
			currentPlayer = player1;
		}
	}

	/**
	 * Returns the Player whose turn currently is.
	 * 
	 * @return
	 */
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}

	/**
	 * Returns true if one of the Players lost.
	 * 
	 * @return
	 */
	public boolean isOver() {
		return player1.getArmy().hasLost() || player2.getArmy().hasLost();
	}

	/**
	 * Starts the game, allowing the Players to attack each other's armies.
	 */
	public void startGame() {
		gameStarted = true;
	}

	/**
	 * Internal operation called when constructing a RomanBattle instance. Builds an
	 * "example" of each predetermined legion and stores it in an array, so the game
	 * may clone the predetermined legions while building a player's army. The file
	 * from which the game reads requires composed legions to be specified below
	 * their components, otherwise the game will behave erratically.
	 * 
	 * @throws IOException
	 */
	private void getPrediterminedLegions() {

		try {
			String filePath = new File("").getAbsolutePath();
			filePath += "/src/main/java/resources/predeterminedLegions.txt";
			FileConverter.convertFC(filePath);
			FileReader fr = new FileReader(filePath);
			BufferedReader reader = new BufferedReader(fr);
			int numberOfLegions = 0;
			String line = reader.readLine();

			while (line != null) {
				numberOfLegions++;
				line = reader.readLine();
			}

			this.predeterminedLegions = new Legion[numberOfLegions];
			this.legionCosts = new int[numberOfLegions];
			fr = new FileReader(filePath);
			reader = new BufferedReader(fr);
			line = reader.readLine();
			numberOfLegions = 0;

			while (line != null) {
				StringTokenizer tokenizer = new StringTokenizer(line, ",");
				String token = tokenizer.nextToken().trim();

				predeterminedLegions[numberOfLegions] = new Legion(token);

				token = tokenizer.nextToken().trim();

				token = createSubLegion(numberOfLegions, token, tokenizer);

				for (int i = 0; i < Integer.parseInt(token); i++) {
					predeterminedLegions[numberOfLegions].addSoldier(new Auxiliary());
				}

				legionCosts[numberOfLegions] += Integer.parseInt(token) * 50;

				token = tokenizer.nextToken().trim();

				for (int i = 0; i < Integer.parseInt(token); i++) {
					predeterminedLegions[numberOfLegions].addSoldier(new Legionnaire());
				}

				legionCosts[numberOfLegions] += Integer.parseInt(token) * 100;

				token = tokenizer.nextToken().trim();

				for (int i = 0; i < Integer.parseInt(token); i++) {
					predeterminedLegions[numberOfLegions].addSoldier(new Centurion());
				}

				legionCosts[numberOfLegions] += Integer.parseInt(token) * 200;

				if (predeterminedLegions[numberOfLegions].countSoldiers() > 0) {
					numberOfLegions++;
				}

				line = reader.readLine();
			}

			fr.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * Internal method, employed by getPredeterminedLegions(). Clones existent
	 * legions into a new composed legion.
	 * 
	 * @param numberOfLegions
	 * @param token
	 * @param tokenizer
	 * @return
	 */
	private String createSubLegion(int numberOfLegions, String token, StringTokenizer tokenizer) {
		try {
			Integer.parseInt(token);
		} catch (NumberFormatException e) {

			for (int i = 0; i < predeterminedLegions.length; i++) {
				if (predeterminedLegions[i].getLegionName().equals(token)) {
					predeterminedLegions[numberOfLegions].addSoldier(predeterminedLegions[i].clone());
					legionCosts[numberOfLegions] += legionCosts[i];
					break;
				}
			}

			token = token.replaceAll(token, tokenizer.nextToken().trim());
			return createSubLegion(numberOfLegions, token, tokenizer);
		}

		return token;
	}
}
