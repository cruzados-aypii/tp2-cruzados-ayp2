package logicFunctionality;

import java.math.BigDecimal;

public class Centurion extends Soldier {

	/**
	 * Creates a new Centurion type Soldier. Starts with 100 health points and has
	 * no priority over other Soldier types to be attacked. Centurion increases the
	 * damage of other Soldiers contained in the same Army as this Soldier by 10%;
	 * this value is cumulative, not multiplicative, with other Centurions' bonuses.
	 */
	public Centurion() {
		super(1);
	}

	@Override
	/**
	 * Attacks the indicated Soldier applying the given multiplier and returns the
	 * damage dealt. Centurion has 1.0 base damage and possesses no special
	 * attacking abilities. Returns 0 if this Soldier is dead.
	 */
	public double attack(Soldier defender, double multiplier) {
		return defender.defend(multiplier);
	}

	@Override
	/**
	 * Receives incoming damage and returns the damage taken. Once at least 100
	 * damage is taken (not received), the Soldier dies and can't attack or defend
	 * again. Centurion has a 50% chance to dodge an incoming attack. Returns 0 if
	 * this Soldier is already dead.
	 */
	public double defend(double incomingDamage) {

		if (Math.random() * 100 <= 50 && this.health.doubleValue() > 0) {
			double damageTaken = 0;

			if (this.health.subtract(new BigDecimal("" + incomingDamage)).doubleValue() < 0) {
				damageTaken = this.health.doubleValue();
				this.health = new BigDecimal("0");
			} else {
				damageTaken = incomingDamage;
				this.health = this.health.subtract(new BigDecimal("" + incomingDamage));
			}

			return damageTaken;
		} else {
			return 0;
		}
	}

}
