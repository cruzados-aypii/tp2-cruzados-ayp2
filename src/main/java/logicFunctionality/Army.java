package logicFunctionality;

import java.math.BigDecimal;

public class Army {

	private BinaryHeap<Soldier> soldiers;
	private BigDecimal damageMultiplier;
	private boolean hasLost;

	/**
	 * Creates an empty Army to store Soldiers for a Player instance.
	 */
	public Army() {
		this.soldiers = new BinaryHeap<Soldier>(2000, true);
		this.damageMultiplier = new BigDecimal("1.0");
		this.hasLost = false;
	}

	/**
	 * Attacks the indicated Army and returns the total damage dealt.
	 * 
	 * @param defendingArmy
	 * @return
	 */
	public double attack(Army defendingArmy) {
		Comparable<Soldier>[] attackers = soldiers.getArray();
		int i = 0;
		BigDecimal damageDealt = new BigDecimal("0");
		Legion lastLegion = null;

		while (i <= soldiers.getLastPosition() && !defendingArmy.hasLost()) {

			if (defendingArmy.getDefender().getPriority() == 1) {
				double centurionHealth = defendingArmy.getHealth();
				double centurionHealthLoss = ((Soldier) attackers[i]).attack(defendingArmy.getDefender(),
						damageMultiplier.doubleValue());
				damageDealt = damageDealt.add(new BigDecimal("" + centurionHealthLoss));

				if (centurionHealth == centurionHealthLoss) {
					defendingArmy.setDamageMultiplier(
							defendingArmy.getDamageMultiplierInBigDecimal().subtract(new BigDecimal(0.1)));
				}

			} else {
				damageDealt = damageDealt.add(new BigDecimal("" + ((Soldier) attackers[i])
						.attack(defendingArmy.getDefender(), damageMultiplier.doubleValue())));
			}

			if (!(attackers[i] instanceof Legion)) {
				i++;
			} else {
				lastLegion = ((Legion) attackers[i]);
				if (lastLegion.getLastAttackPosition() == -1) {
					i++;
				}
			}

			defendingArmy.checkDefender();
		}

		if (lastLegion != null) {
			lastLegion.resetLastAttackPosition();
		}

		return damageDealt.doubleValue();
	}

	/**
	 * Internal operation called in attack() method. Checks the Soldier defending
	 * for removal (if it died) or priority lowering (in case of a Legion losing
	 * it's top-priority Soldiers).
	 */
	private void checkDefender() {
		try {
			if (getDefender() instanceof Legion) {
				soldiers.updateTop();
			}

			if (soldiers.peek().getHealth() == 0) {

				if (soldiers.peek().getPriority() == 1) {
					damageMultiplier.subtract(new BigDecimal("0.1"));
				}

				soldiers.remove();

				if (soldiers.isEmpty()) {
					this.hasLost = true;
				}
			}
		} catch (BinaryHeapEmpty e) {

		}
	}

	/**
	 * Returns the Soldier with the highest priority. May return a Legion whose
	 * defender has the highest priority to be attacked in the Army.
	 * 
	 * @return
	 */
	public Soldier getDefender() {
		try {
			return soldiers.peek();
		} catch (BinaryHeapEmpty e) {
			return null;
		}
	}

	/**
	 * Returns the top-priority Soldier's remaining health points.
	 * 
	 * @return
	 */
	public double getHealth() {
		if (getDefender() != null) {
			return getDefender().getHealth();
		} else {
			return 0;
		}
	}

	/**
	 * Adds the indicated Soldier to he Army. If the Soldier is dead or null, it
	 * won't be added.
	 * 
	 * @param newSoldier
	 */
	public void addSoldier(Soldier newSoldier) {
		if (newSoldier != null && newSoldier.getHealth() > 0) {
			if (newSoldier instanceof Centurion) {
				setDamageMultiplier(damageMultiplier.add(new BigDecimal("0.1")));
			} else if (newSoldier instanceof Legion) {
				setDamageMultiplier(damageMultiplier.add(new BigDecimal(((Legion) newSoldier).calculateDamageBonus())));
			}
			soldiers.insert(newSoldier);
		}
	}

	/**
	 * Returns the total number of Soldiers contained in the Army.
	 * 
	 * @return
	 */
	public int countSoldiers() {
		int totalSoldiers = 0;
		Comparable<Soldier>[] soldierArray = soldiers.getArray();

		for (int i = 0; i <= soldiers.getLastPosition(); i++) {
			if (soldierArray[i] instanceof Legion) {
				totalSoldiers += ((Legion) soldierArray[i]).countSoldiers();
			} else {
				totalSoldiers++;
			}
		}

		return totalSoldiers;
	}

	/**
	 * Returns the total number of Soldiers contained in the army and saves the
	 * amount of Soldiers of each type in the given array. The first three positions
	 * of soldierCounter should be initialized at 0.
	 * 
	 * @param soldierCounter
	 * @return
	 */
	public int countSoldiers(int[] soldierCounter) {
		int totalSoldiers = 0;
		Comparable<Soldier>[] soldierArray = soldiers.getArray();

		for (int i = 0; i <= soldiers.getLastPosition(); i++) {
			Soldier soldier = (Soldier) soldierArray[i];
			if (soldier instanceof Legion) {
				totalSoldiers += ((Legion) soldier).countSoldiers(soldierCounter);
			} else if (soldier.getPriority() == 3) {
				soldierCounter[0]++;
				totalSoldiers++;
			} else if (soldier.getPriority() == 2) {
				soldierCounter[1]++;
				totalSoldiers++;
			} else {
				soldierCounter[2]++;
				totalSoldiers++;
			}
		}
		return totalSoldiers;
	}

	/**
	 * Returns true if this Army has lost.
	 * 
	 * @return
	 */
	public boolean hasLost() {
		return this.hasLost;
	}

	/**
	 * Returns the damage multiplier associated to the amount of Centurions in the
	 * Army.
	 * 
	 * @return
	 */
	public double getDamageMultiplier() {
		return this.damageMultiplier.doubleValue();
	}

	/**
	 * Returns a BigDecimal object representing getDamageMultiplier() value.
	 * 
	 * @return
	 */
	public BigDecimal getDamageMultiplierInBigDecimal() {
		return this.damageMultiplier;
	}

	/**
	 * Sets newMultiplier as the Army's new damage multiplier.
	 * 
	 * @param newMultiplier
	 */
	public void setDamageMultiplier(BigDecimal newMultiplier) {
		this.damageMultiplier = newMultiplier;
	}
}
