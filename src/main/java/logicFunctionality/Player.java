package logicFunctionality;

import java.math.BigDecimal;

public class Player {

	private static int numberOfPlayers;
	private String name;
	private final Army playerArmy;
	private int credits;

	/**
	 * Creates a Player with the given name. Each Player starts with 500 000 credits
	 * and an empty Army. If name is null, a generic name will be provided.
	 * 
	 * @param name
	 */
	public Player(String name) {

		numberOfPlayers++;

		if (name == null || name.equals("")) {
			name = "Player " + numberOfPlayers;
		}

		this.name = name;
		this.playerArmy = new Army();
		this.credits = 500000;
	}

	// Public methods

	/**
	 * Attacks the indicated Player's Army and returns the damage dealt.
	 * 
	 * @param rival
	 * @return
	 */
	public double attackPlayer(Player rival) {
		return this.playerArmy.attack(rival.getArmy());
	}

	@Override
	public String toString() {
		String summary = "";
		int[] soldierCounter = { 0, 0, 0 };
		// First position auxiliaries, second position legionnaires, third position
		// centurions

		summary += String.format("Player " + this.name + "%nTotal soldiers: " + this.playerArmy.countSoldiers(soldierCounter)
				+ "%nTotal health: " + (this.playerArmy.getHealth() - 100 + (100 * this.playerArmy.countSoldiers()))
				+ "%nSpecific soldiers:%n-Auxiliaries: " + soldierCounter[0] + "%n-Legionnaires: " + soldierCounter[1]
				+ "%n-Centurions: " + soldierCounter[2] + "%nCenturion bonus damage: "
				+ ((int) (this.playerArmy.getDamageMultiplier() - 1) * 100) + "%%");

		BigDecimal totalDamage;
		BigDecimal multiplier = this.playerArmy.getDamageMultiplierInBigDecimal();

		totalDamage = new BigDecimal("" + soldierCounter[0]).multiply(new BigDecimal("0.7"));
		totalDamage = (new BigDecimal("" + soldierCounter[1]).multiply(new BigDecimal("1.4"))).add(totalDamage);
		totalDamage = (new BigDecimal("" + soldierCounter[2]).multiply(new BigDecimal("1"))).add(totalDamage);
		totalDamage = totalDamage.multiply(multiplier);

		summary += String.format("%nTotal damage capacity: " + totalDamage.doubleValue() + "%nCredits left: " + this.credits);

		summary = summary.replaceAll("-100", "0");
		return summary;
	}

	// Static methods

	/**
	 * Resets the static int variable used to name Player instances which received a
	 * null parameter.
	 */
	public static void resetNumberOfPlayers() {
		numberOfPlayers = 0;
	}

	/**
	 * Returns the static int variable used to name Player instances which received
	 * a null parameter.
	 * 
	 * @return
	 */
	public static int getNumberOfPlayers() {
		return numberOfPlayers;
	}

	// Getters & setters

	/**
	 * Returns this Player's name.
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns this Player's Army.
	 * 
	 * @return
	 */
	public Army getArmy() {
		return this.playerArmy;
	}

	/**
	 * Returns this Player's current balance.
	 * 
	 * @return
	 */
	public int getCredits() {
		return this.credits;
	}

	/**
	 * Sets the Player's current balance to newBalance.
	 * 
	 * @param newBalance
	 */
	public void setCredits(int newBalance) {
		if (newBalance >= 0) {
			this.credits = newBalance;
		}
	}
}
