package consoleMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import logicFunctionality.Player;
import logicFunctionality.RomanBattle;

public class GameInterface {

	private BufferedReader inputReader;
	private RomanBattle game;

	/**
	 * Constructs an instance of the Roman Battle game.
	 * 
	 * @param br
	 */
	public GameInterface(BufferedReader br) {
		this.inputReader = br;
	}

	/**
	 * Launches the game.
	 */
	public void launch() {
		try {
			// players write their names
			System.out.printf(
					"Player 1: type your name. Be mindful, all characters except %% are valid, but no more than 20.%n%n");

			String name = inputReader.readLine();
			if (name.length() > 20) {
				while (name.length() > 20) {
					System.out.printf("The name can't be longer than 20 characters.%n%n");
					name = inputReader.readLine();
				}
			}

			while (name.contains("%")) {
				System.out.printf("The name can't contain the character '%%'.%n%n");
				name = inputReader.readLine();
			}

			Player player1 = new Player(name);

			System.out.printf(
					"Player 2: type your name. Be mindful, all characters except %% are valid, but no more than 20.%n%n");

			name = inputReader.readLine();
			while (name.length() > 20) {
				System.out.printf("The name can't be longer than 20 characters.%n%n");
				name = inputReader.readLine();
			}

			while (name.toLowerCase().equals(player1.getName().toLowerCase())) {
				System.out.printf("Your name can't be the same as %s's name.%n%n", player1.getName());
				name = inputReader.readLine();
			}

			Player player2 = new Player(name);

			System.out.printf("%s: type -throw to throw the dice.%n%n", player1.getName());

			// players throw dices

			boolean wait = true;
			int player1Dice = 0;

			while (wait) {
				String input = inputReader.readLine();
				if ("-help".equals(input.toLowerCase())) {
					printHelp();
					System.out.printf("%s: type -throw to throw the dice.%n%n", player1.getName());

				} else if ("-throw".equals(input.toLowerCase())) {
					player1Dice = RomanBattle.throwDice();
					System.out.printf("%s got a %d%n%n", player1.getName(), player1Dice);
					wait = false;

				} else {
					printInvalidCommand();
				}
			}

			System.out.printf("%s: type -throw to throw the dice.%n%n", player2.getName());

			wait = true;
			int player2Dice = 0;

			while (wait) {
				String input = inputReader.readLine();
				if ("-help".equals(input.toLowerCase())) {
					printHelp();
					System.out.printf("%s: type -throw to throw the dice.%n%n", player2.getName());

				} else if ("-throw".equals(input.toLowerCase())) {
					player2Dice = RomanBattle.throwDice();
					System.out.printf("%s got a %d%n", player2.getName(), player2Dice);
					if (player1Dice == player2Dice) {
						System.out.println("Draw, second player will start.");
					}
					wait = false;

				} else {
					printInvalidCommand();
				}
			}

			this.game = new RomanBattle(player1, player2, player1Dice > player2Dice);

			// players build their armies

			System.out.printf(
					"Player %s starts. Type -buySoldier or -buyLegion to enter the respective shops. Type -pass once you are"
							+ " done recruiting soldiers%nor type -summary to get a description of your status.%n%n",
					game.getCurrentPlayer().getName());

			wait = true;

			while (wait) {
				String input = inputReader.readLine();

				if ("-help".equals(input.toLowerCase())) {
					printHelp();
					printInitialPhase();

				} else if ("-buysoldier".equals(input.toLowerCase())) {
					buySoldier();
					printInitialPhaseShopExit();

				} else if ("-buylegion".equals(input.toLowerCase())) {
					buyLegion();
					printInitialPhaseShopExit();

				} else if ("-pass".equals(input.toLowerCase())) {

					if (player1.getCredits() < 500000 && player2.getCredits() < 500000) {
						game.startGame();
						wait = false;
					} else if (game.getCurrentPlayer().getCredits() < 500000) {
						System.out.printf("Passing turn...%n%n");
						game.passTurn();
						System.out.printf("It is now %s's turn.%n%n", game.getCurrentPlayer().getName());
					} else {
						System.out.printf("%s: you must recruit at least one soldier.%n%n",
								game.getCurrentPlayer().getName());
					}

				} else if ("-summary".equals(input.toLowerCase())) {
					System.out.println(game.getCurrentPlayer().toString() + System.lineSeparator());

				} else {
					printInvalidCommand();
				}
			}

			// main attack phase

			wait = true;

			System.out.printf(
					"The battle has started and %s has the first move, type -attack to lay siege on the opponent's army. You can"
							+ " -buySoldier%nor -buyLegion at any time during your turn. Type -summary or -help for information.%n",
					game.getCurrentPlayer().getName());

			while (wait) {

				String input = inputReader.readLine();

				if ("-help".equals(input.toLowerCase())) {
					printHelp();
					printAttackPhase();

				} else if ("-buysoldier".equals(input.toLowerCase())) {
					if (!game.isOver()) {
						buySoldier();
						printMainPhaseShopExit();
					} else {
						System.out.printf("The game is over, you can't recruit more soldiers.%n%n");
					}

				} else if ("-buylegion".equals(input.toLowerCase())) {
					if (!game.isOver()) {
						buyLegion();
						printMainPhaseShopExit();
					} else {
						System.out.printf("The game is over, you can't recruit more soldiers.%n%n");
					}

				} else if ("-summary".equals(input.toLowerCase())) {
					System.out.println(game.getCurrentPlayer().toString() + System.lineSeparator());

				} else if ("-attack".equals(input.toLowerCase())) {
					int lostSoldiers = (game.getCurrentPlayer() == player1) ? player2.getArmy().countSoldiers()
							: player1.getArmy().countSoldiers();
					System.out.printf("%s dealt %s damage to %s%n%n", game.getCurrentPlayer().getName(),
							"" + game.playerAttacks(), game.getCurrentPlayer().getName());

					lostSoldiers -= game.getCurrentPlayer().getArmy().countSoldiers();

					System.out.printf("%s lost %s soldiers.%n%n", game.getCurrentPlayer().getName(), "" + lostSoldiers);

					if (game.isOver()) {
						Player loser = game.getCurrentPlayer();
						game.passTurn();
						System.out.printf("%s lost to %s. Type -finish to return to the main menu.%n%n",
								loser.getName(), game.getCurrentPlayer().getName());

					} else {
						System.out.printf("It is now %s's turn.%n%n", game.getCurrentPlayer().getName());
					}

				} else if ("-finish".equals(input.toLowerCase()) && game.isOver()) {
					break;

				} else {
					printInvalidCommand();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Enters the Legion shop.
	 */
	private void buyLegion() {
		try {
			printLegionShop();
			boolean wait = true;

			while (wait) {
				String input = inputReader.readLine();

				if ("-help".equals(input.toLowerCase())) {
					printHelp();
					printLegionShop();

				} else if ("-list".equals(input.toLowerCase())) {
					System.out.printf("%s%n", game.listPredeterminedLegions());

				} else if ("-return".equals(input.toLowerCase())) {
					wait = false;

				} else if ("-credits".equals(input.toLowerCase())) {
					System.out.printf("%s's credits: %s%n%n", game.getCurrentPlayer().getName(),
							game.getCurrentPlayer().getCredits());

				} else if ("-summary".equals(input.toLowerCase())) {
					System.out.println(game.getCurrentPlayer().toString() + System.lineSeparator());

				} else {
					try {
						int credits = game.getCurrentPlayer().getCredits();
						game.buyLegion(Integer.parseInt(input));
						System.out.printf("Credits spent: %s%n%n",
								"" + (credits - game.getCurrentPlayer().getCredits()));
					} catch (NumberFormatException e) {
						printInvalidCommand();
					} catch (ArrayIndexOutOfBoundsException e) {
						System.out.printf("Invalid legion number.%n%n");
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Enters the Soldier shop.
	 */
	private void buySoldier() {
		try {
			printSoldierShop();
			boolean wait = true;

			while (wait) {
				String input = inputReader.readLine();

				if ("-help".equals(input.toLowerCase())) {
					printHelp();
					printSoldierShop();

				} else if ("-return".equals(input.toLowerCase())) {
					wait = false;

				} else if ("-credits".equals(input.toLowerCase())) {
					System.out.printf("%s's credits: %s%n%n", "" + game.getCurrentPlayer().getName(),
							game.getCurrentPlayer().getCredits());

				} else if ("-summary".equals(input.toLowerCase())) {
					System.out.println(game.getCurrentPlayer().toString() + System.lineSeparator());

				} else {
					try {
						StringTokenizer tokenizer = new StringTokenizer(input);
						String soldierType = tokenizer.nextToken();
						String quantity = "";

						if (tokenizer.hasMoreTokens()) {
							quantity = tokenizer.nextToken();

							if ("auxiliary".equals(soldierType.toLowerCase())
									|| "auxiliaries".equals(soldierType.toLowerCase())) {
								addSoldier(1, quantity);

							} else if ("legionnaire".equals(soldierType.toLowerCase())
									|| "legionnaires".equals(soldierType.toLowerCase())) {
								addSoldier(2, quantity);

							} else if ("centurion".equals(soldierType.toLowerCase())
									|| "centurions".equals(soldierType.toLowerCase())) {
								addSoldier(3, quantity);

							} else {
								printInvalidCommand();
							}

						} else {
							printInvalidCommand();
						}
					} catch (NoSuchElementException e) {
						printInvalidCommand();
					}

				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds soldiers to the current player of the game.
	 * 
	 * @param soldierType
	 * @param quantity
	 */
	private void addSoldier(int soldierType, String quantity) {
		try {
			int initialCredits = game.getCurrentPlayer().getCredits();
			if (Integer.parseInt(quantity) <= 10000) {
				game.buySoldiers(soldierType, Integer.parseInt(quantity));
				System.out.printf("Credits spent: %s%n%n",
						"" + (initialCredits - game.getCurrentPlayer().getCredits()));
			}

			if (game.getCurrentPlayer().getCredits() == initialCredits) {
				if (Integer.parseInt(quantity) > 0) {
					System.out.printf("Not enough credits%n%n");

				} else {
					System.out.printf("Can't buy 0 or less soldiers%n%n");
				}
			}
		} catch (NumberFormatException e) {
			printInvalidCommand();
		}
	}

	/**
	 * Prints a message indicating the exit of the shop during the main phase of
	 * Roman Battle.
	 */
	private void printMainPhaseShopExit() {
		System.out.printf("Exitting recruitment shop...%n");
		printAttackPhase();
	}

	/**
	 * Prints a message indicating the exit of the shop during the initial phase of
	 * Roman Battle.
	 */
	private void printInitialPhaseShopExit() {
		System.out.printf("Exitting recruitment shop...%n");
		printInitialPhase();
	}

	/**
	 * Prints the commands relevant to the Soldier shop.
	 */
	private void printSoldierShop() {
		System.out.printf(
				"%s: type -help to see a description of the game (including soldier's specifications),"
						+ " type%n-return to cancel the recruitment, type -credits to check your credits or type the"
						+ " soldier which you want to recruit and the quantity.%nExample: auxiliary 1000%nNote"
						+ " that the soldier type is in lower case. Keep in mind there are no refunds.%n%n",
				game.getCurrentPlayer().getName());
	}

	/**
	 * Prints the commands relevant to the Legion shop.
	 */
	private void printLegionShop() {
		System.out.printf("%s: type -list to see a list of available legions, type -return to cancel the recruitment,"
				+ " type -credits to check%nyour remaining credits or type the number of the legion you wish"
				+ " to buy.%nThere are no refunds.%n%n", game.getCurrentPlayer().getName());
	}

	/**
	 * Prints the commands relevant to the main phase of Roman Battle.
	 */
	private void printAttackPhase() {
		System.out.printf("%s: Type -buySoldier or -buyLegion to enter the respective shops. Type -attack once you are"
				+ " done recruiting soldiers to attack%nor type -summary to get a description of your status.%n%n",
				game.getCurrentPlayer().getName());
	}

	/**
	 * Prints the commands relevant to the initial phase of Roman Battle.
	 */
	private void printInitialPhase() {
		System.out.printf(
				"%s: Type -buySoldier or -buyLegion to enter the respective shops. Type -pass once you are"
						+ " done recruiting soldiers%nor type -summary to get a description of your status.%n%n",
				game.getCurrentPlayer().getName());
	}

	/**
	 * Prints a short description of Roman Battle's rules.
	 */
	public static void printHelp() {
		System.out.printf("In Roman Battle, two players get to build their own massive armies and fight each"
				+ " other. Both players%nstart with 500 000 credits, and can spend them to buy"
				+ " different types of soldiers,%nor even predetermined legions. There are three types of"
				+ " soldiers:%nAuxiliaries: cost 50 credits each, deal 0.7 damage and have a 50%% chance"
				+ " to miss an attack.%nLegionnaires: cost 100 credits each, deal 1.4 damage and have"
				+ " no special abilities or weaknesses.%nCenturions: cost 200 credits each, deal 1.0"
				+ " damage and have a 50%% chance to dodge an attack. Centurions also increase the%ndamage"
				+ " output of the army they belong to by 10%%, stacking acumulatively with other centurions."
				+ "%nA dice is thrown to decide who buys soldiers first, and the player who buys second gets"
				+ " to attack first.%nMore soldiers can be bought if the%nplayer still has credits. Once a"
				+ " player's army runs out of soldiers, even if%ncredits still remain, that player loses.%n%n");
	}

	/**
	 * Prints a message indicating the input of an invalid command.
	 */
	public static void printInvalidCommand() {
		System.out.printf("Invalid command.%n%n");
	}
}
