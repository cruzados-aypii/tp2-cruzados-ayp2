package consoleMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RomanBattleApplication {

	public static void main(String[] args) {
		try {
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
			System.out.printf("Welcome to Roman Battle. Type -start to begin the game or -help to get a quick"
					+ " explanation of the game's mechanics.%nType -exit to close the game.%n%n");

			boolean keepRunning = true;
			while (keepRunning) {
				String input = inputReader.readLine();

				if ("-exit".equals(input)) {
					keepRunning = false;

				} else if ("-help".equals(input)) {
					GameInterface.printHelp();
					printStart();

				} else if ("-start".equals(input)) {
					GameInterface gi = new GameInterface(inputReader);
					gi.launch();
					System.out.printf("Type -start to begin a new game or -exit to close the program.%n%n");

				} else {
					GameInterface.printInvalidCommand();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Prints the commands of the main menu.
	 */
	private static void printStart() {
		System.out.printf("Type -start to begin the game or -help to get a quick explanation of the game's mechanics."
				+ "%nType -exit to close the game.%n%n");
	}
}
